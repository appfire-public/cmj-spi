package com.botronsoft.cmj.spi.appdata;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;

/**
 * Represents an app's object type.
 *
 * @see AppDataObjectEnumerator
 */
public interface AppDataObjectType {

	/**
	 * Returns the id of the specific object type. The id of the type must be unique across all types for this app.
	 *
	 * @return the id of the object type
	 */
	@NotNull
	String getId();

	/**
	 * Returns the name of the specific object type.
	 *
	 * @return the name of the object type.
	 */
	@NotNull
	String getName();

	/**
	 * An icon for the object type. It should be in Base64 format.
	 *
	 * @return the icon of the object type in Base64 format.
	 */
	@Nullable
	String getIcon();
}
