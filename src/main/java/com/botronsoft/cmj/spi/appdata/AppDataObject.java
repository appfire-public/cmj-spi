package com.botronsoft.cmj.spi.appdata;

import java.util.Map;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;

import com.botronsoft.cmj.spi.annotations.PublicSpi;
import com.botronsoft.cmj.spi.configuration.ExportContext;

/**
 * Represents an app's object.
 *
 * @see AppDataObjectEnumerator
 */
@PublicSpi
public interface AppDataObject {
	/**
	 * Returns the id of the app's object type that this object is associated with.
	 * 
	 * @return the id of the app's object type.
	 */
	@NotNull
	String getTypeId();

	/**
	 * Returns the id of the app's object. The id of the object must be unique within its type. A list of the selected objects' ids with
	 * their types' ids will be provided to the app's handler in {@link AppDataHandler#exportData(Map, ExportContext)}
	 * AppDataHandler#exportData(Map&lt;String, List&lt;String&gt;&gt; selectedObjectIds, ExportContext exportContext)}.
	 *
	 * @return the id of the app's object.
	 * @see AppDataHandler
	 */
	@NotNull
	String getObjectId();

	/**
	 * @return the name of the object.
	 */
	@NotNull
	String getName();

	/**
	 * @return the description of the object.
	 */
	@Nullable
	String getDescription();
}
