package com.botronsoft.cmj.spi.appdata;

import java.util.List;

import com.botronsoft.cmj.spi.annotations.PublicSpi;

/**
 * The implementation of this interface is responsible for getting app's object types and objects.
 * <p>
 * In case the annotation-based approach for handler declaration is chosen, the class implementing this interface should be added as a value
 * of annotation {@link com.botronsoft.cmj.spi.annotations.handlers.ОbjectsEnumeratedBy} in the implementation of {@link AppDataHandler}
 */
@PublicSpi
public interface AppDataObjectEnumerator {

	/**
	 * Invoked when an app types are represented during export.
	 * <p>
	 * This method will be called by Configuration Manager for each app implementing {@link AppDataHandler} during export.
	 *
	 * @return list of app object types.
	 */
	List<AppDataObjectType> getObjectTypes();

	/**
	 * Invoked when an app objects are represented during export.
	 * <p>
	 * This method will be called by Configuration Manager for each app implementing {@link AppDataHandler} during export.
	 *
	 * @param typeId
	 *            the id of the {@link AppDataObjectType}
	 * @return list of objects for a specified app type.
	 */
	List<AppDataObject> getObjects(String typeId);
}
