package com.botronsoft.cmj.spi.appdata;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.botronsoft.cmj.spi.annotations.PublicSpi;
import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceCollector;
import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceLookup;
import com.botronsoft.cmj.spi.configuration.ExportContext;
import com.botronsoft.cmj.spi.configuration.ImportContext;

/**
 * Implement this interface to handle export/import for app data.
 * <p>
 * The implementation of this interface is responsible for serializing/deserializing app data and recording references to Jira configuration
 * elements like app filter type option IDs, etc.
 * <p>
 * In case the annotation-based approach for handler declaration is chosen, the implementation of this interface should be annotated with
 * {@link com.botronsoft.cmj.spi.annotations.handlers.ОbjectsEnumeratedBy} to describe app's types and objects that could be
 * exported/imported.
 *
 * @see ConfigurationReferenceCollector
 * @see ConfigurationReferenceLookup
 */
@PublicSpi
public interface AppDataHandler {

	/**
	 * Invoked when an app data is being exported. This method will be called by Configuration Manager for each app with implementation of
	 * this handler.
	 * <p>
	 * Implementers may call {@link com.botronsoft.cmj.spi.configuration.ConfigurationReferenceCollector} methods to handle references to
	 * other configuration objects.
	 *
	 * @param selectedObjectIds
	 *            map of the selected objects for export by the user with their type as a key (see {@link AppDataObjectType#getId()}) and
	 *            object ids as values (see {@link AppDataObject#getObjectId()}).
	 * @param exportContext
	 *            context of the export operation.
	 * @return the exported app data serialized as a string.
	 * @see ExportContext
	 */
	Optional<String> exportData(Map<String, List<String>> selectedObjectIds, ExportContext exportContext);

	/**
	 * Invoked when an app data is being imported. This method will be called by Configuration Manager for each app with implementation of
	 * this handler.
	 * <p>
	 * Implementers can call {@link com.botronsoft.cmj.spi.configuration.ConfigurationReferenceLookup} methods to retrieve the respective
	 * matching app objects referred with {@link com.botronsoft.cmj.spi.configuration.ConfigurationReferenceCollector}.
	 *
	 * @param data
	 *            the app data as serialized by {@link AppDataHandler#exportData(Map, ExportContext)
	 *            AppDataHandler#exportData(Map&lt;String, List&lt;String&gt;&gt; selectedObjectIds, ExportContext exportContext)}.
	 * @param importContext
	 *            context of the import operation.
	 * @see ImportContext
	 */
	void importData(String data, ImportContext importContext);

}
