package com.botronsoft.cmj.spi.issue;

import java.util.Optional;
import java.util.Set;

import com.botronsoft.cmj.spi.annotations.PublicSpi;

import com.atlassian.jira.project.Project;

/**
 * Interface for migrating issue data. This interface will be called whenever a project with issues is being exported or imported.
 */
@PublicSpi
public interface IssueDataHandler {

	/**
	 * Exports issue data for the provided project. Implementors are advised to optimize the performance of this method by first checking if
	 * anything needs to be exported for the project at all.
	 *
	 * @param project
	 *            the project for which data is exported.
	 * @param issueIds
	 *            a set of all exported issue ids - this could contain all issues in the project or just a subset.
	 * @param context
	 *            the export context.
	 * @return an {@link Optional} with the exported issue data or {@link Optional#empty} if the app does not store any issue data for this
	 *         project.
	 */
	Optional<String> exportIssueData(Project project, Set<Long> issueIds, IssueExportContext context);

	/**
	 * Imports issue data for the provided project.
	 *
	 * @param data
	 *            the data which was exported by the {@link #exportIssueData(Project, Set, IssueExportContext)})}) method.
	 * @param project
	 *            the project for which data is imported.
	 * @param context
	 *            the import context.
	 */
	void importIssueData(String data, Project project, IssueImportContext context);
}
