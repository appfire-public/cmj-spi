package com.botronsoft.cmj.spi.issue;

import com.botronsoft.cmj.spi.annotations.PublicApi;
import com.botronsoft.cmj.spi.configuration.AppMappingContext;
import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceLookup;

/**
 * Represents the context of the import operation. Contains API instances relevant to currently running import operation (i.e. during the
 * deployment of a snapshot with issue data).
 *
 * @see ConfigurationReferenceLookup
 * @see IssueDataLookup
 * @see AppMappingContext
 */
@PublicApi
public interface IssueImportContext {

	/**
	 * Returns {@link ConfigurationReferenceLookup} for current import operation (i.e. during the deployment of a snapshot). Will return the
	 * same instance if called more than once during a single import.
	 *
	 * @return {@link ConfigurationReferenceLookup} for current import operation.
	 */
	ConfigurationReferenceLookup getReferenceLookup();

	/**
	 * Returns {@link IssueDataLookup} for current import operation (i.e. during the deployment of a snapshot). Will return the same
	 * instance if called more than once during a single import.
	 *
	 * @return {@link IssueDataLookup} for current import operation.
	 */
	IssueDataLookup getIssueDataLookup();

	/**
	 * Returns {@link AppMappingContext} for current import operation (i.e. during the deployment of a snapshot). Will return the same
	 * instance if called more than once during a single import.
	 *
	 * * Use this to lookup source to target ids of app specific objects being migrated in the current operation. Make sure that you have
	 * recorded all the needed mappings in handlers that take care for app specific objects migration.
	 *
	 * @return {@link AppMappingContext} for current import operation.
	 */
	AppMappingContext getAppMappingContext();
}
