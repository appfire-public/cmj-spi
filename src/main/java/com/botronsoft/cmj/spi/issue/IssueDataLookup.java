package com.botronsoft.cmj.spi.issue;

import java.util.Optional;

import com.botronsoft.cmj.spi.annotations.PublicApi;

/**
 * Interface which can be used to lookup the new identifiers of different issue objects on import.
 */
@PublicApi
public interface IssueDataLookup {

	/**
	 * Lookup the id of an issue on the target system which corresponds to the provided source id. Note that only issues that are created on
	 * the target system within the scope of this import will be available for lookup regardless of whether the issue is included in the
	 * snapshot.
	 *
	 * @param sourceId
	 *            the id of the issue on the source system.
	 * @return an {@link Optional} of the id of the issue on target when the issue is created on target within the scope of the current
	 *         import and an empty {@link Optional} otherwise.
	 *
	 */
	Optional<Long> lookupIssueId(Long sourceId);

	/**
	 * Lookup the id of a comment on the target system which corresponds to the provided source id. Note that only comments on issues that
	 * are created on the target system within the scope of this import will be available for lookup regardless of whether the containing
	 * issue is included in the snapshot.
	 *
	 * @param sourceId
	 *            the id of the comment on the source system.
	 * @return an {@link Optional} of the id of the comment on target when the containing issue is created on target within the scope of the
	 *         current import and an empty {@link Optional} otherwise.
	 *
	 */
	Optional<Long> lookupCommentId(Long sourceId);

	/**
	 * Lookup the id of an attachment on the target system which corresponds to the provided source id. Note that only attachments to issues
	 * that are created on the target system within the scope of this import will be available for lookup regardless of whether the
	 * containing issue is included in the snapshot.
	 *
	 * @param sourceId
	 *            the id of the attachment on the source system.
	 * @return an {@link Optional} of the id of the attachment on target when the containing issue is created on target within the scope of
	 *         the current import and an empty {@link Optional} otherwise.
	 *
	 */
	Optional<Long> lookupAttachmentId(Long sourceId);

	/**
	 * Lookup the id of a worklog on the target system which corresponds to the provided source id. Note that only worklogs for issues that
	 * are created on the target system within the scope of this import will be available for lookup regardless of whether the containing
	 * issue is included in the snapshot.
	 *
	 * @param sourceId
	 *            the id of the attachment on the source system.
	 * @return an {@link Optional} of the id of the worklog on target when the containing issue is created on target within the scope of the
	 *         current import and an empty {@link Optional} otherwise.
	 *
	 */
	Optional<Long> lookupWorklogId(Long sourceId);
}
