package com.botronsoft.cmj.spi.issue;

import com.botronsoft.cmj.spi.annotations.PublicApi;
import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceCollector;

/**
 * Represents the context of the issue export operation. Contains API instances relevant to currently running export operation (i.e. when
 * creating a snapshot with issue data).
 *
 * @see ConfigurationReferenceCollector
 */
@PublicApi
public interface IssueExportContext {

	/**
	 * Returns {@link ConfigurationReferenceCollector} for current export operation (i.e. when creating a snapshot). Will return the same
	 * instance if called more than once during a single export.
	 *
	 * @return {@link ConfigurationReferenceCollector} for current export operation.
	 */
	ConfigurationReferenceCollector getReferenceCollector();
}
