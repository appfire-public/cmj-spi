package com.botronsoft.cmj.spi.issue;

import java.util.Optional;

import com.botronsoft.cmj.spi.annotations.PublicSpi;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.CustomFieldType;
import com.atlassian.jira.issue.fields.CustomField;

/**
 * Interface for migrating custom field values. Values for custom fields can be stored in two places - the current value of the issue and
 * also values in the issue history. For this purpose there are methods in this interface for converting both current values and history
 * values.
 *
 * @param <T>
 *            the type of the value - this should be the same as T (transport object type) that is declared in the {@link CustomFieldType}
 *            implementation in the app.
 */
@PublicSpi
public interface CustomFieldDataHandler<T> {

	/**
	 * Transforms an issue value for the given custom field to a serialized String. This method will be called by Configuration Manager for
	 * each issue if it contains a value for this field.
	 * 
	 * @param customField
	 *            the custom field for which the value is exported. The field is of type for which this handler has been registered.
	 * @param issue
	 *            the issue for which the value is exported.
	 * @param value
	 *            the value itself.
	 * @param issueExportContext
	 *            the context of the export operation.
	 * @return an {@link Optional} containing the exported value serialized as a String. This value will be passed to the
	 *         CustomFieldDataHandler{@link #transformIssueValueForImport(CustomField, Issue, String, IssueImportContext)} method on import.
	 */
	Optional<String> transformIssueValueForExport(CustomField customField, Issue issue, T value, IssueExportContext issueExportContext);

	/**
	 * Transforms an issue history value for the given custom field to a serialized String. This method will be called by Configuration
	 * Manager for each value in a history entry for this field.
	 * 
	 * @param customField
	 *            the custom field for which the value is exported. The field is of type for which this handler has been registered.
	 * @param issue
	 *            the issue for which the value is exported.
	 * @param value
	 *            the value from the history entry.
	 * @param valueString
	 *            the value string from the history entry.
	 * @param issueExportContext
	 *            the context of the export operation.
	 * @return an {@link Optional} containing the exported value serialized as a String. This value will be passed to
	 *         {@link CustomFieldType#getChangelogValue(CustomField, Object)} and
	 *         {@link CustomFieldType#getChangelogString(CustomField, Object)} when the history entry is imported.
	 */
	Optional<String> transformChangeLogValueForExport(CustomField customField, Issue issue, String value, String valueString,
			IssueExportContext issueExportContext);

	/**
	 * Transform a previously serialized value for import. The resulting type must be the one declared in the {@link CustomFieldType}
	 * implementation in the app.
	 * 
	 * @param customField
	 *            the custom field for which the value is imported. The field is of type for which this handler has been registered.
	 * @param issue
	 *            the issue for which the value is imported.
	 * @param value
	 *            the value itself.
	 * @param issueImportContext
	 *            the context of the import operation.
	 * @return an {@link Optional} containing the value for this custom field type.
	 */
	Optional<T> transformIssueValueForImport(CustomField customField, Issue issue, String value, IssueImportContext issueImportContext);

}