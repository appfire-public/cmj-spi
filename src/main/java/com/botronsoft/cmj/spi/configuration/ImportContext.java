package com.botronsoft.cmj.spi.configuration;

import com.botronsoft.cmj.spi.annotations.PublicApi;

/**
 * Represents the context of the import operation. Contains API instances relevant to currently running import operation (i.e. during the
 * deployment of a snapshot).
 *
 * @see ConfigurationReferenceLookup
 * @see AppMappingContext
 */
@PublicApi
public interface ImportContext {

	/**
	 * Returns {@link ConfigurationReferenceLookup} for current import operation (i.e. during the deployment of a snapshot). Will return the
	 * same instance if called more than once during a single import.
	 *
	 * @return {@link ConfigurationReferenceLookup} for current import operation.
	 */
	ConfigurationReferenceLookup getReferenceLookup();

	/**
	 * Returns {@link AppMappingContext} for current import operation (i.e. during the deployment of a snapshot). Will return the same
	 * instance if called more than once during a single import.
	 *
	 * Use this in SPI handlers that import app specific objects to record mappings of source to target ids of those objects so that other
	 * SPI handlers of the same app can use this mapping to replace id references in the configuration or issue data they import.
	 *
	 * @return {@link AppMappingContext} for current import operation.
	 */
	AppMappingContext getAppMappingContext();

	/**
	 * Returns the import mode for the current import operation.
	 * 
	 * @return the current {@link ImportMode}.
	 */
	ImportMode getImportMode();
}
