package com.botronsoft.cmj.spi.configuration;

import com.botronsoft.cmj.spi.annotations.PublicApi;

/**
 * Represents the context of the export operation. Contains API instances relevant to currently running export operation (i.e. when creating
 * a snapshot).
 *
 * @see ConfigurationReferenceCollector
 */
@PublicApi
public interface ExportContext {

	/**
	 * Returns {@link ConfigurationReferenceCollector} for current export operation (i.e. when creating a snapshot). Will return the same
	 * instance if called more than once during a single export.
	 *
	 * @return {@link ConfigurationReferenceCollector} for current export operation.
	 */
	ConfigurationReferenceCollector getReferenceCollector();
}
