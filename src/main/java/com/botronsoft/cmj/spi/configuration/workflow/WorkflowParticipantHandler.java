package com.botronsoft.cmj.spi.configuration.workflow;

import com.botronsoft.cmj.spi.annotations.PublicSpi;
import com.botronsoft.cmj.spi.configuration.ExportContext;
import com.botronsoft.cmj.spi.configuration.ImportContext;

import java.util.Map;

/**
 * Implement this interface to handle export/import for custom configuration data referenced in the arguments of a workflow transition
 * participant (condition, validator or post-function).
 */
@PublicSpi
public interface WorkflowParticipantHandler {

	/**
	 * Invoked when a configuration is being exported. This method will be called by Configuration Manager for each workflow transition
	 * participant (condition, validator or post-function) which is handled by this handler.
	 *
	 * Implementers may call {@link com.botronsoft.cmj.spi.configuration.ConfigurationReferenceCollector} methods to handle references to
	 * other configuration objects.
	 *
	 * @param className
	 *            the fully qualified class name of the currently exported workflow transition participant.
	 * @param args
	 *            a map of arguments that are configured for the currently exported workflow transition participant.
	 * @param exportContext
	 *            the context of the export operation.
	 * @return a map of the transformed arguments - these can be the same arguments which were passed to this method or enhanced set of
	 *         arguments. This map will be passed to {@link #transformArgumentsForImport(String, Map, ImportContext)} when the configuration
	 *         is being deployed.
	 *
	 * @see ExportContext
	 */
	Map<String, String> transformArgumentsForExport(String className, Map<String, String> args, ExportContext exportContext);

	/**
	 * Invoked when a configuration is being imported. This method will be called by Configuration Manager for each workflow transition
	 * participant (condition, validator or post-function) which is handled by this handler.
	 *
	 * Implementers can call {@link com.botronsoft.cmj.spi.configuration.ConfigurationReferenceLookup} methods to retrieve the respective
	 * matching configuration objects referred with {@link com.botronsoft.cmj.spi.configuration.ConfigurationReferenceCollector}.
	 *
	 * @param className
	 *            the fully qualified class name of the currently imported workflow transition participant.
	 * @param args
	 *            the map which was returned by the invocation of {@link #transformArgumentsForExport(String, Map, ExportContext)}.
	 * @param importContext
	 *            the context of the import operation.
	 * @return a map of the transformed arguments - these are the arguments that will be effectively stored for the corresponding transition
	 *         participant.
	 *
	 * @see ImportContext
	 */
	Map<String, String> transformArgumentsForImport(String className, Map<String, String> args, ImportContext importContext);

}
