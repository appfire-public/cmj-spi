package com.botronsoft.cmj.spi.configuration;

import com.botronsoft.cmj.spi.annotations.PublicApi;

/**
 * Enum which represents the mode for the import operation. SPI configuration handler implementations may choose to do different things
 * based on the import mode - for example in merge mode, SPI implementations may merge the new configuration with what currently exists on
 * the target system, while in restore mode they can completely overwrite the current data on the target system. Use {@link ImportContext}
 * to retrieve the current import mode.
 * 
 * @see ImportContext#getImportMode()
 */
@PublicApi
public enum ImportMode {

	/**
	 * Configuration Manager deploys a project snapshot or a system snapshot in "merge" mode.
	 */
	MERGE,

	/**
	 * Configuration Manager deploys a system snapshot in "restore" mode.
	 */
	RESTORE;
}
