package com.botronsoft.cmj.spi.configuration.global;

import java.util.Optional;

import com.botronsoft.cmj.spi.annotations.PublicSpi;
import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceCollector;
import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceLookup;
import com.botronsoft.cmj.spi.configuration.ExportContext;
import com.botronsoft.cmj.spi.configuration.ImportContext;
import com.botronsoft.cmj.spi.configuration.ImportMode;

/**
 * Implement this interface to handle export/import for custom configuration which is global for the Jira instance.
 *
 * The implementation of this interface is responsible for serializing/deserializing global configuration data and recording references to
 * Jira configuration elements like users, groups, custom fields, etc.
 *
 * @see ConfigurationReferenceCollector
 * @see ConfigurationReferenceLookup
 */
@PublicSpi
public interface GlobalConfigurationHandler {

	/**
	 * Invoked when a configuration is being exported. This method will be called by Configuration Manager when the app which implements it
	 * is selected for export. Implementors may return {@link Optional#empty()} if there is no configuration to be exported.
	 *
	 * Implementers may call {@link ConfigurationReferenceCollector} methods to handle references to other configuration objects.
	 *
	 * @param exportContext
	 *            context of the export operation.
	 * @return an {@link Optional} with the exported configuration or {@link Optional#empty} if the app does not store any global
	 *         configuration.
	 *
	 * @see ExportContext
	 */
	Optional<String> exportConfiguration(ExportContext exportContext);

	/**
	 * Invoked when a configuration is being imported. Use * {@link ImportContext#getImportMode()} to distinguish between merge and restore.
	 *
	 * Implementers can call {@link ConfigurationReferenceLookup} methods to retrieve the respective matching configuration objects referred
	 * with {@link ConfigurationReferenceCollector}.
	 *
	 * @param configuration
	 *            the configuration as serialized by {@link GlobalConfigurationHandler#exportConfiguration(ExportContext)}.
	 * @param importContext
	 *            context of the import operation.
	 *
	 * @see ImportContext
	 * @see ImportMode
	 */
	void importConfiguration(String configuration, ImportContext importContext);

	/**
	 * Invoked when configuration needs to be deleted. This method will be called by Configuration Manager when app data is deleted - this
	 * can happen only when system snapshot is deployed in "restore" mode and the snapshot does not contain any app data.
	 */
	void deleteConfiguration();
}
