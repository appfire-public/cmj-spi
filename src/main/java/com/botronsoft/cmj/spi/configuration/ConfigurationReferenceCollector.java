package com.botronsoft.cmj.spi.configuration;

import com.botronsoft.cmj.spi.annotations.PublicApi;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.event.type.EventType;
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.fields.config.FieldConfigScheme;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutScheme;
import com.atlassian.jira.issue.fields.screen.FieldScreenScheme;
import com.atlassian.jira.issue.fields.screen.issuetype.IssueTypeScreenScheme;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.link.IssueLinkType;
import com.atlassian.jira.issue.priority.Priority;
import com.atlassian.jira.issue.resolution.Resolution;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.security.IssueSecurityLevel;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.scheme.Scheme;
import com.atlassian.jira.security.roles.ProjectRole;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.query.Query;

/**
 * Interface which can be invoked on export - collects references to JIRA configuration elements by a given key.
 *
 * On import collected objects are matched using Configuration Manager's matching algorithms. Matching objects can be retrieved on a JIRA
 * target instance by the given key using the {@link ConfigurationReferenceLookup}.
 *
 * It's the callers responsibility to provide unique keys - keys must be unique for the type of the configuration object - for example the
 * same key can be used for a status and a priority. Any duplicate keys will override the referred object.
 *
 * Null keys are not allowed. Null references will produce an audit and JIRA log warning and will not halt the export procedure.
 *
 * @see ConfigurationReferenceLookup
 */
@PublicApi
public interface ConfigurationReferenceCollector {

	/**
	 * Collects a {@link Field} reference. Field can be either a {@link com.atlassian.jira.issue.fields.CustomField} or a {@link Field}
	 * (representing a system field).
	 *
	 * A list of System ("built-in") fields can be found here:
	 * https://confluence.atlassian.com/adminjiraserver073/configuring-built-in-fields-861253234.html
	 *
	 * Null references are ignored and will produce an audit and JIRA log warnings.
	 *
	 * @param key
	 *            A key uniquely identifying referred object. Caller should make sure keys are unique across method calls, unless caller's
	 *            intent is overriding the reference.
	 * @param reference
	 *            The referred JIRA {@link Field}
	 */
	void collectField(String key, Field reference);

	/**
	 * Collects an {@link Option} reference.
	 *
	 * Null references are ignored and will produce an audit and JIRA log warnings.
	 *
	 * @param key
	 *            A key uniquely identifying referred object. Caller should make sure keys are unique across method calls, unless caller's
	 *            intent is overriding the reference.
	 * @param reference
	 *            The referred JIRA {@link Option}
	 */
	void collectFieldOption(String key, Option reference);

	/**
	 * Collects a {@link FieldConfigScheme} reference. Null references are ignored and will produce an audit and JIRA log warnings.
	 *
	 * Implementing classes will try to lookup the referred {@link FieldConfigScheme} via the JIRA API.
	 *
	 * @param key
	 *            A key uniquely identifying referred object. Caller should make sure keys are unique across method calls, unless caller's
	 *            intent is overriding the reference.
	 * @param fieldConfigScheme
	 *            The referred JIRA {@link FieldConfigScheme}
	 *
	 */
	void collectFieldContext(String key, FieldConfigScheme fieldConfigScheme);

	/**
	 * Collects an {@link ApplicationUser} reference. Null references are ignored and will produce an audit and JIRA log warnings.
	 *
	 * Implementing classes will try to lookup the referred {@link ApplicationUser} via the JIRA API.
	 *
	 * @param key
	 *            A key uniquely identifying referred object. Caller should make sure keys are unique across method calls, unless caller's
	 *            intent is overriding the reference.
	 * @param reference
	 *            The referred JIRA {@link ApplicationUser}
	 */
	void collectUser(String key, ApplicationUser reference);

	/**
	 * Collects a {@link Group} reference. Null references are ignored and will produce an audit and JIRA log warnings.
	 *
	 * Implementing classes will try to lookup the referred {@link Group} via the JIRA API.
	 *
	 * @param key
	 *            A key uniquely identifying referred object. Caller should make sure keys are unique across method calls, unless caller's
	 *            intent is overriding the reference.
	 * @param reference
	 *            The referred JIRA {@link Group}
	 */
	void collectGroup(String key, Group reference);

	/**
	 * Collects an {@link IssueType} reference. Null references are ignored and will produce an audit and JIRA log warnings.
	 *
	 * Implementing classes will try to lookup the referred {@link IssueType} via the JIRA API.
	 *
	 * @param key
	 *            A key uniquely identifying referred object. Caller should make sure keys are unique across method calls, unless caller's
	 *            intent is overriding the reference.
	 * @param reference
	 *            The referred JIRA {@link IssueType}
	 */
	void collectIssueType(String key, IssueType reference);

	/**
	 * Collects an {@link IssueLinkType} reference. Null references are ignored and will produce an audit and JIRA log warnings.
	 *
	 * Implementing classes will try to lookup the referred {@link IssueLinkType} via the JIRA API.
	 *
	 * @param key
	 *            A key uniquely identifying referred object. Caller should make sure keys are unique across method calls, unless caller's
	 *            intent is overriding the reference.
	 * @param reference
	 *            The referred JIRA {@link IssueLinkType}
	 */
	void collectIssueLinkType(String key, IssueLinkType reference);

	/**
	 * Collects a {@link Status} reference. Null references are ignored and will produce an audit and JIRA log warnings.
	 *
	 * Implementing classes will try to lookup the referred {@link Status} via the JIRA API.
	 *
	 * @param key
	 *            A key uniquely identifying referred object. Caller should make sure keys are unique across method calls, unless caller's
	 *            intent is overriding the reference.
	 * @param reference
	 *            The referred JIRA {@link Status}
	 */
	void collectStatus(String key, Status reference);

	/**
	 * Collects a {@link Resolution} reference. Null references are ignored and will produce an audit and JIRA log warnings.
	 *
	 * Implementing classes will try to lookup the referred {@link Resolution} via the JIRA API.
	 *
	 * @param key
	 *            A key uniquely identifying referred object. Caller should make sure keys are unique across method calls, unless caller's
	 *            intent is overriding the reference.
	 * @param reference
	 *            The referred JIRA {@link Resolution}
	 */
	void collectResolution(String key, Resolution reference);

	/**
	 * Collects a {@link Priority} reference. Null references are ignored and will produce an audit and JIRA log warnings.
	 *
	 * Implementing classes will try to lookup the referred {@link Priority} via the JIRA API.
	 *
	 * @param key
	 *            A key uniquely identifying referred object. Caller should make sure keys are unique across method calls, unless caller's
	 *            intent is overriding the reference.
	 * @param reference
	 *            The referred JIRA {@link Priority}
	 */
	void collectPriority(String key, Priority reference);

	/**
	 * Collects a {@link EventType} reference. Null references are ignored and will produce an audit and JIRA log warnings.
	 *
	 * Implementing classes will try to lookup the referred {@link EventType} via the JIRA API.
	 *
	 * @param key
	 *            A key uniquely identifying referred object. Caller should make sure keys are unique across method calls, unless caller's
	 *            intent is overriding the reference.
	 * @param reference
	 *            The referred JIRA {@link EventType}
	 */
	void collectEventType(String key, EventType reference);

	/**
	 * Collects a {@link SearchRequest} reference. Null references are ignored and will produce an audit and JIRA log warnings.
	 *
	 * Implementing classes will try to lookup the referred {@link SearchRequest} via the JIRA API. The referred {@link SearchRequest}
	 * should be either global or the user initiating the export should have access to it, otherwise lookup will fail.
	 *
	 * In some cases a {@link SearchRequest} might have "broken" JQL (e.x. if it refers a project which has been deleted). In these cases an
	 * audit warning will be added.
	 *
	 * @param key
	 *            A key uniquely identifying referred object. Caller should make sure keys are unique across method calls, unless caller's
	 *            intent is overriding the reference.
	 * @param reference
	 *            The referred JIRA {@link SearchRequest}
	 */
	void collectSearchRequest(String key, SearchRequest reference);

	/**
	 * Collects a {@link Query} reference. A query is treated as a JQL string which is checked for validity. In case the query refers an
	 * object that is no-longer present or has an invalid syntax an audit warning will be created and the export process will continue.
	 *
	 * Null references are ignored and will produce audit and JIRA log warnings.
	 *
	 * @param key
	 *            A key uniquely identifying referred object. Caller should make sure keys are unique across method calls, unless caller's
	 *            intent is overriding the reference.
	 * @param query
	 *            The referred JIRA {@link Query}
	 */
	void collectQuery(String key, Query query);

	/**
	 * Collects a {@link Project} reference. Null references are ignored and will produce an audit and JIRA log warnings.
	 *
	 * Implementing classes will try to lookup the referred {@link Project} via the JIRA API.
	 *
	 * @param key
	 *            A key uniquely identifying referred object. Caller should make sure keys are unique across method calls, unless caller's
	 *            intent is overriding the reference.
	 * @param project
	 *            The referred JIRA {@link Project}
	 */
	void collectProject(String key, Project project);

	/**
	 * Collects a {@link Version} reference. Null references are ignored and will produce an audit and JIRA log warnings.
	 *
	 * Implementing classes will try to lookup the referred {@link Version} via the JIRA API.
	 *
	 * @param key
	 *            A key uniquely identifying referred object. Caller should make sure keys are unique across method calls, unless caller's
	 *            intent is overriding the reference.
	 * @param projectVersion
	 *            The referred JIRA {@link Version}
	 */
	void collectProjectVersion(String key, Version projectVersion);

	/**
	 * Collects a {@link ProjectComponent} reference. Null references are ignored and will produce an audit and JIRA log warnings.
	 *
	 * Implementing classes will try to lookup the referred {@link ProjectComponent} via the JIRA API.
	 *
	 * @param key
	 *            A key uniquely identifying referred object. Caller should make sure keys are unique across method calls, unless caller's
	 *            intent is overriding the reference.
	 * @param projectComponent
	 *            The referred JIRA {@link ProjectComponent}
	 */
	void collectProjectComponent(String key, ProjectComponent projectComponent);

	/**
	 * Collects a {@link ProjectRole} reference. Null references are ignored and will produce an audit and JIRA log warnings.
	 *
	 * Implementing classes will try to lookup the referred {@link ProjectRole} via the JIRA API.
	 *
	 * @param key
	 *            A key uniquely identifying referred object. Caller should make sure keys are unique across method calls, unless caller's
	 *            intent is overriding the reference.
	 * @param projectRole
	 *            The referred JIRA {@link ProjectRole}
	 */
	void collectProjectRole(String key, ProjectRole projectRole);

	/**
	 * Collects a Request Type reference. Null references are ignored and will produce an audit and JIRA log warnings.
	 *
	 * @param key
	 *            A key uniquely identifying referred object. Caller should make sure keys are unique across method calls, unless caller's
	 *            intent is overriding the reference.
	 * @param requestTypeId
	 *            The referred JIRA Request type id
	 */
	void collectRequestType(String key, Long requestTypeId);

	/**
	 * Collects a {@link IssueSecurityLevel} reference. Null references are ignored and will produce an audit and JIRA log warnings.
	 *
	 * Implementing classes will try to lookup the referred {@link IssueSecurityLevel} via the JIRA API.
	 *
	 * @param key
	 *            A key uniquely identifying referred object. Caller should make sure keys are unique across method calls, unless caller's
	 *            intent is overriding the reference.
	 * @param securityLevel
	 *            The referred JIRA {@link IssueSecurityLevel}
	 */
	void collectSecurityLevel(String key, IssueSecurityLevel securityLevel);

	/**
	 * Collects a Sprint reference. Null references are ignored and will produce an audit and JIRA log warnings.
	 *
	 * In case if JIRA Software is not installed it will produce an audit and JIRA log warning.
	 *
	 * @param key
	 *            A key uniquely identifying referred object. Caller should make sure keys are unique across method calls, unless caller's
	 *            intent is overriding the reference.
	 * @param sprintId
	 *            The referred JIRA Sprint id
	 */
	void collectSprint(String key, Long sprintId);

	/**
	 * Collects an Agile Board reference. Null references are ignored and will produce an audit and JIRA log warnings.
	 *
	 * In case if JIRA Software is not installed it will produce an audit and JIRA log warning.
	 *
	 * @param key
	 *            A key uniquely identifying referred object. Caller should make sure keys are unique across method calls, unless caller's
	 *            intent is overriding the reference.
	 * @param agileBoardId
	 *            The referred JIRA Agile Board id
	 */
	void collectAgileBoard(String key, Long agileBoardId);

	/**
	 * Collects a {@link JiraWorkflow} reference. Null references are ignored and will produce an audit and JIRA log warnings.
	 *
	 * Implementing classes will try to lookup the referred {@link JiraWorkflow} via the JIRA API.
	 *
	 * @param key
	 *            A key uniquely identifying referred object. Caller should make sure keys are unique across method calls, unless caller's
	 *            intent is overriding the reference.
	 * @param jiraWorkflow
	 *            The referred JIRA {@link JiraWorkflow}
	 */
	void collectJiraWorkflow(String key, JiraWorkflow jiraWorkflow);

	/**
	 * Collects a {@link Scheme} reference. Null references are ignored and will produce an audit and JIRA log warnings.
	 *
	 * Implementing classes will try to lookup the referred {@link Scheme} via the JIRA API.
	 *
	 * @param key
	 *            A key uniquely identifying referred object. Caller should make sure keys are unique across method calls, unless caller's
	 *            intent is overriding the reference.
	 * @param workflowScheme
	 *            The referred JIRA {@link Scheme}
	 */
	void collectWorkflowScheme(String key, Scheme workflowScheme);

	/**
	 * Collects a {@link FieldConfigScheme} reference. Null references are ignored and will produce an audit and JIRA log warnings.
	 *
	 * Implementing classes will try to lookup the referred {@link FieldConfigScheme} via the JIRA API.
	 *
	 * @param key
	 *            A key uniquely identifying referred object. Caller should make sure keys are unique across method calls, unless caller's
	 *            intent is overriding the reference.
	 * @param issueTypeScheme
	 *            The referred JIRA {@link FieldConfigScheme}
	 */
	void collectIssueTypeScheme(String key, FieldConfigScheme issueTypeScheme);

	/**
	 * Collects a {@link IssueTypeScreenScheme} reference. Null references are ignored and will produce an audit and JIRA log warnings.
	 *
	 * Implementing classes will try to lookup the referred {@link IssueTypeScreenScheme} via the JIRA API.
	 *
	 * @param key
	 *            A key uniquely identifying referred object. Caller should make sure keys are unique across method calls, unless caller's
	 *            intent is overriding the reference.
	 * @param issueTypeScreenScheme
	 *            The referred JIRA {@link IssueTypeScreenScheme}
	 */
	void collectIssueTypeScreenScheme(String key, IssueTypeScreenScheme issueTypeScreenScheme);

	/**
	 * Collects a {@link FieldScreenScheme} reference. Null references are ignored and will produce an audit and JIRA log warnings.
	 *
	 * Implementing classes will try to lookup the referred {@link FieldScreenScheme} via the JIRA API.
	 *
	 * @param key
	 *            A key uniquely identifying referred object. Caller should make sure keys are unique across method calls, unless caller's
	 *            intent is overriding the reference.
	 * @param fieldScreenScheme
	 *            The referred JIRA {@link FieldScreenScheme}
	 */
	void collectFieldScreenScheme(String key, FieldScreenScheme fieldScreenScheme);

	/**
	 * Collects a {@link FieldLayoutScheme} reference. Null references are ignored and will produce an audit and JIRA log warnings.
	 *
	 * Implementing classes will try to lookup the referred {@link FieldLayoutScheme} via the JIRA API.
	 *
	 * @param key
	 *            A key uniquely identifying referred object. Caller should make sure keys are unique across method calls, unless caller's
	 *            intent is overriding the reference.
	 * @param fieldLayoutScheme
	 *            The referred JIRA {@link FieldLayoutScheme}
	 *
	 */
	void collectFieldLayoutScheme(String key, FieldLayoutScheme fieldLayoutScheme);
}
