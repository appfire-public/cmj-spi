package com.botronsoft.cmj.spi.configuration.project;

import java.util.Optional;

import com.botronsoft.cmj.spi.annotations.PublicSpi;
import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceCollector;
import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceLookup;
import com.botronsoft.cmj.spi.configuration.ExportContext;
import com.botronsoft.cmj.spi.configuration.ImportContext;
import com.botronsoft.cmj.spi.configuration.ImportMode;

import com.atlassian.jira.project.Project;

/**
 * Implement this interface to handle export/import for custom configuration related to a {@link com.atlassian.jira.project.Project}.
 *
 * The implementation of this interface is responsible for serializing/deserializing project configuration data and recording references to
 * Jira configuration elements like users, groups, custom fields, etc.
 *
 * @see ConfigurationReferenceCollector
 * @see ConfigurationReferenceLookup
 */
@PublicSpi
public interface ProjectConfigurationHandler {

	/**
	 * Invoked when a configuration is being exported. This method will be called by Configuration Manager for each exported project.
	 * Implementors may return {@link Optional#empty()} if there is no configuration to be exported for this particular project.
	 *
	 * Implementers may call {@link com.botronsoft.cmj.spi.configuration.ConfigurationReferenceCollector} methods to handle references to
	 * other configuration objects.
	 *
	 * @param project
	 *            the currently exported {@link Project}.
	 * @param exportContext
	 *            context of the export operation.
	 * @return an {@link Optional} with the exported configuration or {@link Optional#empty} if the app does not store any configuration for
	 *         this project.
	 *
	 * @see ExportContext
	 */
	Optional<String> exportConfiguration(Project project, ExportContext exportContext);

	/**
	 * Invoked when a configuration is being imported. This method will be called by Configuration Manager for each imported project. Use
	 * {@link ImportContext#getImportMode()} to distinguish between merge and restore.
	 *
	 * Implementers can call {@link com.botronsoft.cmj.spi.configuration.ConfigurationReferenceLookup} methods to retrieve the respective
	 * matching configuration objects referred with {@link com.botronsoft.cmj.spi.configuration.ConfigurationReferenceCollector}.
	 *
	 * @param configuration
	 *            the configuration as serialized by {@link ProjectConfigurationHandler#exportConfiguration(Project, ExportContext)}.
	 * @param project
	 *            the currently imported {@link Project} instance.
	 * @param importContext
	 *            context of the import operation.
	 *
	 * @see ImportContext
	 * @see ImportMode
	 */
	void importConfiguration(String configuration, Project project, ImportContext importContext);

	/**
	 * Invoked when configuration needs to be deleted. This method will be called by Configuration Manager when a project is deleted during
	 * deployment so any configuration stored by the app and associated with this project can also be deleted. A project can be deleted only
	 * when system snapshot is deployed in "restore" mode and the snapshot does not contain the project.
	 *
	 * @param project
	 *            the deleted {@link Project} instance.
	 */
	void deleteConfiguration(Project project);
}
