package com.botronsoft.cmj.spi.configuration;

import java.util.Optional;

import com.botronsoft.cmj.spi.annotations.PublicApi;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.event.type.EventType;
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.fields.config.FieldConfigScheme;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutScheme;
import com.atlassian.jira.issue.fields.screen.FieldScreenScheme;
import com.atlassian.jira.issue.fields.screen.issuetype.IssueTypeScreenScheme;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.link.IssueLinkType;
import com.atlassian.jira.issue.priority.Priority;
import com.atlassian.jira.issue.resolution.Resolution;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.security.IssueSecurityLevel;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.scheme.Scheme;
import com.atlassian.jira.security.roles.ProjectRole;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.query.Query;

/**
 * Interface which an be invoked on import - looks up an object by its key. Intended to be used in conjunction with
 * {@link ConfigurationReferenceCollector}.
 *
 * Methods return JIRA target instance objects that match objects referred by the {@link ConfigurationReferenceCollector}. Keys are
 * maintained across source and target JIRA instances.
 *
 * Methods will return {@link Optional#empty()} if CMJ fails to find a matching object via the JIRA API. In these cases a detailed error
 * will be logged in the JIRA log.
 *
 * @see ConfigurationReferenceCollector
 */
@PublicApi
public interface ConfigurationReferenceLookup {

	/**
	 * Looks up a JIRA {@link Field} by the given key. Returned field can be either a {@link CustomField} or a system (built-in)
	 * {@link Field} depending on type referred via {@link ConfigurationReferenceCollector#collectField(String, Field)}.
	 *
	 * {@link Optional#empty()} is returned in case JIRA API lookup fails or if given key was not used in
	 * {@link ConfigurationReferenceCollector}
	 *
	 * @param key
	 *            Key used to refer field in {@link ConfigurationReferenceCollector#collectField(String, Field)} call
	 * @return Matching field or {@link Optional#empty()} if field JIRA API lookup fails or if given key was not used in
	 *         {@link ConfigurationReferenceCollector}
	 */
	Optional<Field> lookupField(String key);

	/**
	 * Looks up a JIRA {@link Option} by the given key. Returned custom field option that can be a {@link Option}.
	 *
	 * {@link Optional#empty()} is returned in case JIRA API lookup fails or if given key was not used in
	 * {@link ConfigurationReferenceCollector}
	 *
	 * @param key
	 *            Key used to refer field in {@link ConfigurationReferenceCollector#collectFieldOption(String, Option)} call
	 * @return Matching field or {@link Optional#empty()} if field JIRA API lookup fails or if given key was not used in
	 *         {@link ConfigurationReferenceCollector}
	 */
	Optional<Option> lookupFieldOption(String key);

	/**
	 * Produces a JIRA {@link FieldConfigScheme} which corresponds to the {@link FieldConfigScheme} stored by the given key on export
	 *
	 * {@link Optional#empty()} is returned in case JIRA API lookup fails or if given key was not used in
	 * {@link ConfigurationReferenceCollector}
	 *
	 * @param key
	 *            Key used to refer custom field context in
	 *            {@link ConfigurationReferenceCollector#collectFieldContext(String, FieldConfigScheme)} call
	 * @return Matching custom field context or {@link Optional#empty()} if JIRA API lookup fails or if given key was not used in
	 *         {@link ConfigurationReferenceCollector}
	 */
	Optional<FieldConfigScheme> lookupFieldContext(String key);

	/**
	 * Looks up a JIRA {@link ApplicationUser} by the given key.
	 *
	 * {@link Optional#empty()} is returned in case JIRA API lookup fails or if given key was not used in
	 * {@link ConfigurationReferenceCollector}
	 *
	 * @param key
	 *            Key used to refer user in {@link ConfigurationReferenceCollector#collectUser(String, ApplicationUser)} call
	 * @return Matching user or {@link Optional#empty()} if JIRA API lookup fails or if given key was not used in
	 *         {@link ConfigurationReferenceCollector}
	 */
	Optional<ApplicationUser> lookupUser(String key);

	/**
	 * Looks up a JIRA {@link Group} by the given key.
	 *
	 * {@link Optional#empty()} is returned in case JIRA API lookup fails or if given key was not used in
	 * {@link ConfigurationReferenceCollector}
	 *
	 * @param key
	 *            Key used to refer filed in {@link ConfigurationReferenceCollector#collectGroup(String, Group)} call
	 * @return Matching group or {@link Optional#empty()} if JIRA API lookup fails or if given key was not used in
	 *         {@link ConfigurationReferenceCollector}
	 */
	Optional<Group> lookupGroup(String key);

	/**
	 * Looks up a JIRA {@link IssueType} by the given key.
	 *
	 * {@link Optional#empty()} is returned in case JIRA API lookup fails or if given key was not used in
	 * {@link ConfigurationReferenceCollector}
	 *
	 * @param key
	 *            Key used to refer issue type in {@link ConfigurationReferenceCollector#collectIssueType(String, IssueType)} call
	 * @return Matching user or {@link Optional#empty()} if JIRA API lookup fails or if given key was not used in
	 *         {@link ConfigurationReferenceCollector}
	 */
	Optional<IssueType> lookupIssueType(String key);

	/**
	 * Looks up a JIRA {@link IssueLinkType} by the given key.
	 *
	 * {@link Optional#empty()} is returned in case JIRA API lookup fails or if given key was not used in
	 * {@link ConfigurationReferenceCollector}
	 *
	 * @param key
	 *            Key used to refer issue link type in {@link ConfigurationReferenceCollector#collectIssueLinkType(String, IssueLinkType)}
	 *            call
	 * @return Matching user or {@link Optional#empty()} if JIRA API lookup fails or if given key was not used in
	 *         {@link ConfigurationReferenceCollector}
	 */
	Optional<IssueLinkType> lookupIssueLinkType(String key);

	/**
	 * Looks up a JIRA {@link Status} by the given key.
	 *
	 * {@link Optional#empty()} is returned in case JIRA API lookup fails or if given key was not used in
	 * {@link ConfigurationReferenceCollector}
	 *
	 * @param key
	 *            Key used to refer status in {@link ConfigurationReferenceCollector#collectStatus(String, Status)} call
	 * @return Matching user or {@link Optional#empty()} if JIRA API lookup fails or if given key was not used in
	 *         {@link ConfigurationReferenceCollector}
	 */
	Optional<Status> lookupStatus(String key);

	/**
	 * Looks up a JIRA {@link Resolution} by the given key.
	 *
	 * {@link Optional#empty()} is returned in case JIRA API lookup fails or if given key was not used in
	 * {@link ConfigurationReferenceCollector}
	 *
	 * @param key
	 *            Key used to refer resolution in {@link ConfigurationReferenceCollector#collectResolution(String, Resolution)} call
	 * @return Matching user or {@link Optional#empty()} if JIRA API lookup fails or if given key was not used in
	 *         {@link ConfigurationReferenceCollector}
	 */
	Optional<Resolution> lookupResolution(String key);

	/**
	 * Looks up a JIRA {@link Priority} by the given key.
	 *
	 * {@link Optional#empty()} is returned in case JIRA API lookup fails or if given key was not used in
	 * {@link ConfigurationReferenceCollector}
	 *
	 * @param key
	 *            Key used to refer priority in {@link ConfigurationReferenceCollector#collectPriority(String, Priority)} call
	 * @return Matching user or {@link Optional#empty()} if JIRA API lookup fails or if given key was not used in
	 *         {@link ConfigurationReferenceCollector}
	 */
	Optional<Priority> lookupPriority(String key);

	/**
	 * Looks up a JIRA {@link EventType} by the given key.
	 *
	 * {@link Optional#empty()} is returned in case JIRA API lookup fails or if given key was not used in
	 * {@link ConfigurationReferenceCollector}
	 *
	 * @param key
	 *            Key used to refer priority in {@link ConfigurationReferenceCollector#collectEventType(String, EventType)} call
	 * @return Matching user or {@link Optional#empty()} if JIRA API lookup fails or if given key was not used in
	 *         {@link ConfigurationReferenceCollector}
	 */
	Optional<EventType> lookupEventType(String key);

	/**
	 * Looks up a JIRA {@link SearchRequest} by the given key.
	 *
	 * {@link Optional#empty()} is returned in case JIRA API lookup fails, in case calling user doesn't have access to the referred
	 * {@link SearchRequest} or if given key was not used in {@link ConfigurationReferenceCollector}
	 *
	 * @param key
	 *            Key used to refer filter in {@link ConfigurationReferenceCollector#collectSearchRequest(String, SearchRequest)} call
	 * @return Matching user or {@link Optional#empty()} if JIRA API lookup fails or if given key was not used in
	 *         {@link ConfigurationReferenceCollector}
	 */
	Optional<SearchRequest> lookupSearchRequest(String key);

	/**
	 * Produces a JIRA {@link Query} which corresponds to the {@link Query} stored by the given key on export.
	 *
	 * {@link Optional#empty()} is returned in case JIRA API lookup fails or if given key was not used in
	 * {@link ConfigurationReferenceCollector}
	 *
	 * @param key
	 *            Key used to refer JQL string in {@link ConfigurationReferenceCollector#collectQuery(String, Query)} call
	 * @return Matching user or {@link Optional#empty()} if JIRA API lookup fails or if given key was not used in
	 *         {@link ConfigurationReferenceCollector}
	 */
	Optional<Query> lookupQuery(String key);

	/**
	 * Produces a JIRA {@link Project} which corresponds to the {@link Project} stored by the given key on export
	 *
	 * {@link Optional#empty()} is returned in case JIRA API lookup fails or if given key was not used in
	 * {@link ConfigurationReferenceCollector}
	 *
	 * @param key
	 *            Key used to refer project in {@link ConfigurationReferenceCollector#collectProject(String, Project)} call
	 * @return Matching project or {@link Optional#empty()} if JIRA API lookup fails or if given key was not used in
	 *         {@link ConfigurationReferenceCollector}
	 */
	Optional<Project> lookupProject(String key);

	/**
	 * Produces a JIRA {@link Version} which corresponds to the {@link Version} stored by the given key on export
	 *
	 * {@link Optional#empty()} is returned in case JIRA API lookup fails or if given key was not used in
	 * {@link ConfigurationReferenceCollector}
	 *
	 * @param key
	 *            Key used to refer project version in {@link ConfigurationReferenceCollector#collectProjectVersion(String, Version)} call
	 * @return Matching project version or {@link Optional#empty()} if JIRA API lookup fails or if given key was not used in
	 *         {@link ConfigurationReferenceCollector}
	 */
	Optional<Version> lookupProjectVersion(String key);

	/**
	 * Produces a JIRA {@link ProjectComponent} which corresponds to the {@link ProjectComponent} stored by the given key on export
	 *
	 * {@link Optional#empty()} is returned in case JIRA API lookup fails or if given key was not used in
	 * {@link ConfigurationReferenceCollector}
	 *
	 * @param key
	 *            Key used to refer project component in
	 *            {@link ConfigurationReferenceCollector#collectProjectComponent(String, ProjectComponent)} call
	 * @return Matching project component or {@link Optional#empty()} if JIRA API lookup fails or if given key was not used in
	 *         {@link ConfigurationReferenceCollector}
	 */
	Optional<ProjectComponent> lookupProjectComponent(String key);

	/**
	 * Produces a JIRA {@link ProjectRole} which corresponds to the {@link ProjectRole} stored by the given key on export
	 *
	 * {@link Optional#empty()} is returned in case JIRA API lookup fails or if given key was not used in
	 * {@link ConfigurationReferenceCollector}
	 *
	 * @param key
	 *            Key used to refer project role in {@link ConfigurationReferenceCollector#collectProjectRole(String, ProjectRole)} call
	 * @return Matching project role or {@link Optional#empty()} if JIRA API lookup fails or if given key was not used in
	 *         {@link ConfigurationReferenceCollector}
	 */
	Optional<ProjectRole> lookupProjectRole(String key);

	/**
	 * Produces a Request Type id which corresponds to id of the request type stored by the given key on export
	 *
	 * {@link Optional#empty()} is returned in case JIRA API lookup fails or if given key was not used in
	 * {@link ConfigurationReferenceCollector}
	 *
	 * @param key
	 *            Key used to refer request type id in {@link ConfigurationReferenceCollector#collectRequestType(String, Long)} call
	 * @return Matching request type id or {@link Optional#empty()} if JIRA API lookup fails or if given key was not used in
	 *         {@link ConfigurationReferenceCollector}
	 */
	Optional<Long> lookupRequestType(String key);

	/**
	 * Produces a JIRA {@link IssueSecurityLevel} which corresponds to the {@link IssueSecurityLevel} stored by the given key on export
	 *
	 * {@link Optional#empty()} is returned in case JIRA API lookup fails or if given key was not used in
	 * {@link ConfigurationReferenceCollector}
	 *
	 * @param key
	 *            Key used to refer security level in
	 *            {@link ConfigurationReferenceCollector#collectSecurityLevel(String, IssueSecurityLevel)} call
	 * @return Matching security level or {@link Optional#empty()} if JIRA API lookup fails or if given key was not used in
	 *         {@link ConfigurationReferenceCollector}
	 */
	Optional<IssueSecurityLevel> lookupSecurityLevel(String key);

	/**
	 * Produces a Sprint id which corresponds to id of the sprint stored by the given key on export
	 *
	 * {@link Optional#empty()} is returned in case JIRA API lookup fails or if given key was not used in
	 * {@link ConfigurationReferenceCollector}
	 *
	 * @param key
	 *            Key used to refer sprint id in {@link ConfigurationReferenceCollector#collectSprint(String, Long)} } call
	 * @return Matching sprint id or {@link Optional#empty()} if JIRA API lookup fails or if given key was not used in
	 *         {@link ConfigurationReferenceCollector} or in case if JIRA Software is not installed.
	 */
	Optional<Long> lookupSprint(String key);

	/**
	 * Produces an Agile Board id which corresponds to id of the agile board stored by the given key on export
	 *
	 * {@link Optional#empty()} is returned in case JIRA API lookup fails or if given key was not used in
	 * {@link ConfigurationReferenceCollector}
	 *
	 * @param key
	 *            Key used to refer agile board id in {@link ConfigurationReferenceCollector#collectAgileBoard(String, Long)} } call
	 * @return Matching agile board id or {@link Optional#empty()} if JIRA API lookup fails or if given key was not used in
	 *         {@link ConfigurationReferenceCollector} or in case if JIRA Software is not installed.
	 */
	Optional<Long> lookupAgileBoard(String key);

	/**
	 * Produces a JIRA {@link JiraWorkflow} which corresponds to the {@link JiraWorkflow} stored by the given key on export
	 *
	 * {@link Optional#empty()} is returned in case JIRA API lookup fails or if given key was not used in
	 * {@link ConfigurationReferenceCollector}
	 *
	 * @param key
	 *            Key used to refer workflow in {@link ConfigurationReferenceCollector#collectJiraWorkflow(String, JiraWorkflow)} call
	 * @return Matching workflow or {@link Optional#empty()} if JIRA API lookup fails or if given key was not used in
	 *         {@link ConfigurationReferenceCollector}
	 */
	Optional<JiraWorkflow> lookupJiraWorkflow(String key);

	/**
	 * Produces a JIRA {@link Scheme} which corresponds to the {@link Scheme} stored by the given key on export
	 *
	 * {@link Optional#empty()} is returned in case JIRA API lookup fails or if given key was not used in
	 * {@link ConfigurationReferenceCollector}
	 *
	 * @param key
	 *            Key used to refer workflow scheme in {@link ConfigurationReferenceCollector#collectWorkflowScheme(String, Scheme)} call
	 * @return Matching workflow scheme or {@link Optional#empty()} if JIRA API lookup fails or if given key was not used in
	 *         {@link ConfigurationReferenceCollector}
	 */
	Optional<Scheme> lookupWorkflowScheme(String key);

	/**
	 * Produces a JIRA {@link FieldConfigScheme} which corresponds to the {@link FieldConfigScheme} stored by the given key on export
	 *
	 * {@link Optional#empty()} is returned in case JIRA API lookup fails or if given key was not used in
	 * {@link ConfigurationReferenceCollector}
	 *
	 * @param key
	 *            Key used to refer issue type scheme in
	 *            {@link ConfigurationReferenceCollector#collectIssueTypeScheme(String, FieldConfigScheme)} call
	 * @return Matching issue type scheme or {@link Optional#empty()} if JIRA API lookup fails or if given key was not used in
	 *         {@link ConfigurationReferenceCollector}
	 */
	Optional<FieldConfigScheme> lookupIssueTypeScheme(String key);

	/**
	 * Produces a JIRA {@link IssueTypeScreenScheme} which corresponds to the {@link IssueTypeScreenScheme} stored by the given key on
	 * export
	 *
	 * {@link Optional#empty()} is returned in case JIRA API lookup fails or if given key was not used in
	 * {@link ConfigurationReferenceCollector}
	 *
	 * @param key
	 *            Key used to refer issue type screen scheme in
	 *            {@link ConfigurationReferenceCollector#collectIssueTypeScreenScheme(String, IssueTypeScreenScheme)} call
	 * @return Matching issue type screen scheme or {@link Optional#empty()} if JIRA API lookup fails or if given key was not used in
	 *         {@link ConfigurationReferenceCollector}
	 */
	Optional<IssueTypeScreenScheme> lookupIssueTypeScreenScheme(String key);

	/**
	 * Produces a JIRA {@link FieldScreenScheme} which corresponds to the {@link FieldScreenScheme} stored by the given key on export
	 *
	 * {@link Optional#empty()} is returned in case JIRA API lookup fails or if given key was not used in
	 * {@link ConfigurationReferenceCollector}
	 *
	 * @param key
	 *            Key used to refer field screen scheme in
	 *            {@link ConfigurationReferenceCollector#collectFieldScreenScheme(String, FieldScreenScheme)} call
	 * @return Matching field screen scheme or {@link Optional#empty()} if JIRA API lookup fails or if given key was not used in
	 *         {@link ConfigurationReferenceCollector}
	 */
	Optional<FieldScreenScheme> lookupFieldScreenScheme(String key);

	/**
	 * Produces a JIRA {@link FieldLayoutScheme} which corresponds to the {@link FieldLayoutScheme} stored by the given key on export
	 *
	 * {@link Optional#empty()} is returned in case JIRA API lookup fails or if given key was not used in
	 * {@link ConfigurationReferenceCollector}
	 *
	 * @param key
	 *            Key used to refer field layout scheme in
	 *            {@link ConfigurationReferenceCollector#collectFieldLayoutScheme(String, FieldLayoutScheme)} call
	 * @return Matching field layout scheme or {@link Optional#empty()} if JIRA API lookup fails or if given key was not used in
	 *         {@link ConfigurationReferenceCollector}
	 */
	Optional<FieldLayoutScheme> lookupFieldLayoutScheme(String key);
}
