package com.botronsoft.cmj.spi.configuration.field;

import com.botronsoft.cmj.spi.annotations.PublicSpi;
import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceCollector;
import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceLookup;
import com.botronsoft.cmj.spi.configuration.ExportContext;
import com.botronsoft.cmj.spi.configuration.ImportContext;

import com.atlassian.jira.issue.fields.config.FieldConfigScheme;

/**
 * Implement this interface to handle export/import for custom configuration data used by a
 * {@link com.atlassian.jira.issue.fields.CustomField}.
 * 
 * The implementation of this interface is responsible for serializing/deserializing custom field configuration data and recording
 * references to Jira configuration elements like workflows, users, groups, other custom fields, etc.
 *
 * @see ConfigurationReferenceCollector
 * @see ConfigurationReferenceLookup
 */
@PublicSpi
public interface CustomFieldConfigurationHandler {

	/**
	 * Invoked when a configuration is being exported. This method will be called by Configuration Manager for each context of the currently
	 * exported field.
	 *
	 * Implementers may call {@link com.botronsoft.cmj.spi.configuration.ConfigurationReferenceCollector} methods to handle references to
	 * other configuration objects.
	 *
	 * @param customFieldContext
	 *            the currently exported {@link FieldConfigScheme}.
	 * @param exportContext
	 *            context of the export operation.
	 * @return the exported configuration serialized as a string.
	 * 
	 * @see ExportContext
	 */
	String exportConfiguration(FieldConfigScheme customFieldContext, ExportContext exportContext);

	/**
	 * Invoked when a configuration is being imported. This method will be called by Configuration Manager for each context of the currently
	 * imported field.
	 *
	 * Implementers can call {@link com.botronsoft.cmj.spi.configuration.ConfigurationReferenceLookup} methods to retrieve the respective
	 * matching configuration objects referred with {@link com.botronsoft.cmj.spi.configuration.ConfigurationReferenceCollector}.
	 *
	 * @param configuration
	 *            the configuration as serialized by
	 *            {@link CustomFieldConfigurationHandler#exportConfiguration(FieldConfigScheme, ExportContext)}.
	 * @param customFieldContext
	 *            the currently imported {@link FieldConfigScheme} instance.
	 * @param importContext
	 *            context of the import operation.
	 *
	 * @see ImportContext
	 */
	void importConfiguration(String configuration, FieldConfigScheme customFieldContext, ImportContext importContext);

	/**
	 * Invoked when configuration needs to be deleted. This method will be called by Configuration Manager when a custom field context is
	 * deleted so any configuration stored by the app and associated with this context can also be deleted.
	 *
	 * @param customFieldContext
	 *            the deleted {@link FieldConfigScheme} instance.
	 */
	void deleteConfiguration(FieldConfigScheme customFieldContext);
}
