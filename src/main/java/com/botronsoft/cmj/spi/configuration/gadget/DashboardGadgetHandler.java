package com.botronsoft.cmj.spi.configuration.gadget;

import java.util.Map;

import com.botronsoft.cmj.spi.annotations.PublicSpi;
import com.botronsoft.cmj.spi.configuration.ExportContext;
import com.botronsoft.cmj.spi.configuration.ImportContext;

/**
 * Implement this interface to handle export/import of user preferences in a dashboard gadget or item.
 */
@PublicSpi
public interface DashboardGadgetHandler {

	/**
	 * Invoked when a configuration is being exported. This method will be called by Configuration Manager for each dashboard gadget or item
	 * which is handled by this handler.
	 * <p>
	 * Implementers may call {@link com.botronsoft.cmj.spi.configuration.ConfigurationReferenceCollector} methods to handle references to
	 * other configuration objects.
	 *
	 * @param uriOrModuleKey
	 *            the URI of the currently exported dashboard gadget or the module key of the currently exported dashboard item
	 * @param userPreferences
	 *            a map of user preferences that are configured for the currently exported dashboard gadget.
	 * @param exportContext
	 *            the context of the export operation.
	 * @return a map of the transformed user preferences - these can be the same user preferences which were passed to this method or
	 *         enhanced set of user preferences. This map will be passed to
	 *         {@link #transformUserPreferencesForImport(String, Map, ImportContext)} when the configuration is being deployed.
	 * @see ExportContext
	 */
	Map<String, String> transformUserPreferencesForExport(String uriOrModuleKey, Map<String, String> userPreferences,
			ExportContext exportContext);

	/**
	 * Invoked when a configuration is being imported. This method will be called by Configuration Manager for each dashboard gadget or item
	 * which is handled by this handler.
	 * <p>
	 * Implementers can call {@link com.botronsoft.cmj.spi.configuration.ConfigurationReferenceLookup} methods to retrieve the respective
	 * matching configuration objects referred with {@link com.botronsoft.cmj.spi.configuration.ConfigurationReferenceCollector}.
	 *
	 * @param uriOrModuleKey
	 *            the URI of the currently exported dashboard gadget or the module key of the currently exported dashboard item
	 * @param userPreferences
	 *            the map which was returned by the invocation of {@link #transformUserPreferencesForExport(String, Map, ExportContext)}.
	 * @param importContext
	 *            the context of the import operation.
	 * @return a map of the transformed user preferences - these are the user preferences that will be effectively stored for the
	 *         corresponding dashboard gadget.
	 * @see ImportContext
	 */
	Map<String, String> transformUserPreferencesForImport(String uriOrModuleKey, Map<String, String> userPreferences,
			ImportContext importContext);

}
