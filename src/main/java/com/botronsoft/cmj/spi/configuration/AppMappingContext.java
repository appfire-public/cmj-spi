package com.botronsoft.cmj.spi.configuration;

import java.util.Optional;

import com.botronsoft.cmj.spi.annotations.PublicApi;
import com.botronsoft.cmj.spi.configuration.project.ProjectConfigurationHandler;
import com.botronsoft.cmj.spi.issue.IssueDataHandler;

/**
 * Interface that allows SPI implementations to record and lookup source to target id mappings of app configuration objects during import.
 * If an SPI handler creates a new configuration object during import, it can record the mapping between the old id on the source system and
 * the new id on the target system so that other SPI handlers of the same app can use this mapping to replace id references in the
 * configuration or issue data they import. An example would be a {@link ProjectConfigurationHandler} which imports some objects that are
 * later referenced by data imported in an {@link IssueDataHandler}.
 */
@PublicApi
public interface AppMappingContext {

	/**
	 * Records a mapping between source and target id for objects of the given type. Both source id and target id are of type
	 * {@link String}.
	 *
	 * @param type
	 *            the type of the object - this is provided by the app handlers and represents a category of configuration objects, the same
	 *            type must be used for lookup.
	 * @param sourceId
	 *            the source id of the object.
	 * @param targetId
	 *            the target id of the object.
	 */
	void recordMapping(String type, String sourceId, String targetId);

	/**
	 * Records a mapping between source and target id for objects of the given type. Both source id and target id are of type {@link Long}.
	 * 
	 * @param type
	 *            the type of the object - this is provided by the app handlers and represents a category of configuration objects, the same
	 *            * type must be used for lookup.
	 * @param sourceId
	 *            the source id of the object.
	 * @param targetId
	 *            the target id of the object.
	 */
	void recordMapping(String type, Long sourceId, Long targetId);

	/**
	 * Returns an {@link Optional} of the target id of an app specific object if such mapping was previously recorded or an empty
	 * {@link Optional} otherwise.
	 *
	 * @param type
	 *            the type of the object - this is provided by the app handlers and represents a category of configuration objects, the same
	 *            type must be used when recording the mapping.
	 * @param sourceId
	 *            the source id of the object.
	 * @return an {@link Optional} of the target id of an app specific object if such mapping was previously recorded or an empty
	 *         {@link Optional} otherwise.
	 */
	Optional<String> lookupMapping(String type, String sourceId);

	/**
	 * Returns an {@link Optional} of the target id of an app specific object if such mapping was previously recorded or an empty
	 * {@link Optional} otherwise.
	 *
	 * @param type
	 *            the type of the object - this is provided by the app handlers and represents a category of configuration objects, the same
	 *            type must be used when recording the mapping.
	 * @param sourceId
	 *            the source id of the object.
	 * @return an {@link Optional} of the target id of an app specific object if such mapping was previously recorded or an empty
	 *         {@link Optional} otherwise.
	 */
	Optional<Long> lookupMapping(String type, Long sourceId);
}
