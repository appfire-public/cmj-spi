package com.botronsoft.cmj.spi.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import com.atlassian.annotations.PublicApi;

/**
 * The annotated element is part of a product's SPI contract with apps.
 * <p>
 * This element is designed for apps to <em>implement</em>.
 * <p>
 * Clients of <code>@PublicSpi</code> can expect that <b>code compiled against a given version will remain binary compatible with later
 * versions of the <code>@PublicSpi</code></b> as per Configuration Manager's API policy.
 * <p/>
 * Note: <code>@PublicSpi</code> interfaces and classes are specifically designed to be implemented/extended by clients. Hence, the
 * guarantee of binary compatibility is different to that of <code>@PublicApi</code> elements (if an element is both <code>@PublicApi</code>
 * and <code>@PublicSpi</code>, both guarantees apply).
 * <p/>
 *
 * @see PublicApi
 */
@Documented
@Retention(RetentionPolicy.CLASS)
public @interface PublicSpi {
}
