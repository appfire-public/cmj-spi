package com.botronsoft.cmj.spi.annotations.handlers;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.botronsoft.cmj.spi.annotations.PublicSpi;
import com.botronsoft.cmj.spi.configuration.gadget.DashboardGadgetHandler;

/**
 * This annotation can be used on classes which implement the {@link DashboardGadgetHandler} interface in case the annotation-based approach
 * for handler declaration is chosen. Used in conjunction with the {@link ConfigurationManagerSpiHandler} annotation. This annotation can be
 * used multiple times on a single class.
 */
@PublicSpi
@Documented
@Target(value = ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Repeatable(DashboardItems.class)
public @interface HandlesDashboardItem {

	/**
	 * @return the module key of the dashboard item which is handled by the annotated implementation of {@link DashboardGadgetHandler}.
	 *         Multiple {@link HandlesDashboardGadget} annotations may be defined for a single handler.
	 */
	String key();
}
