package com.botronsoft.cmj.spi.annotations.handlers;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.botronsoft.cmj.spi.annotations.PublicSpi;

/**
 * This annotation can be used to specify that the annotated type is a Configuration Manager SPI handler implementation. If annotation-based
 * approach is used for declaring handlers, only implementations with this annotation will be discoverable by Configuration Manager.
 */
@PublicSpi
@Documented
@Target(value = ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface ConfigurationManagerSpiHandler {

}
