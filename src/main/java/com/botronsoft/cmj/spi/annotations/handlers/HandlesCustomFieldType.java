package com.botronsoft.cmj.spi.annotations.handlers;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.botronsoft.cmj.spi.annotations.PublicSpi;
import com.botronsoft.cmj.spi.configuration.field.CustomFieldConfigurationHandler;
import com.botronsoft.cmj.spi.issue.CustomFieldDataHandler;

/**
 * This annotation must be used on classes which implement the {@link CustomFieldConfigurationHandler} or {@link CustomFieldDataHandler}
 * interface in case the annotation-based approach for handler declaration is chosen. Used in conjunction with the
 * {@link ConfigurationManagerSpiHandler} annotation.
 */
@PublicSpi
@Documented
@Target(value = ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface HandlesCustomFieldType {

	/**
	 * 
	 * @return the full key of the custom field type which is handled by the annotated implementation of
	 *         {@link CustomFieldConfigurationHandler} or {@link CustomFieldDataHandler}.
	 */
	String typeKey();
}
