package com.botronsoft.cmj.spi.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * The annotated element is part of the API contract with other apps.
 * <p>
 * This element is designed for apps to <em>consume</em> (call its methods).
 * <p>
 * Clients of <code>@PublicApi</code> can expect that <b>code compiled against a given version will remain binary compatible with later
 * versions of the <code>@PublicApi</code></b> as per Configuration Manager's API policy <b>as long as the client does not implement/extend
 * <code>@PublicApi</code> interfaces or classes</b>.
 * <p/>
 * Note: since <code>@PublicApi</code> interfaces and classes are not designed to be implemented or extended by clients, we may perform
 * certain types of binary-incompatible changes to these classes and interfaces, but these will not affect well-behaved clients that do not
 * extend/implement these types (in general, only classes and interfaces annotated with {@link com.botronsoft.cmj.spi.annotations.PublicSpi}
 * are safe to extend/implement).
 *
 * @see PublicSpi
 */
@Documented
@Retention(RetentionPolicy.CLASS)
public @interface PublicApi {
}
