package com.botronsoft.cmj.spi.annotations.handlers;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.botronsoft.cmj.spi.annotations.PublicSpi;
import com.botronsoft.cmj.spi.appdata.AppDataObjectEnumerator;
import com.botronsoft.cmj.spi.configuration.workflow.WorkflowParticipantHandler;

/**
 * This annotation can be used on classes which implement the {@link com.botronsoft.cmj.spi.appdata.AppDataHandler} interface in case the annotation-based
 * approach for handler declaration is chosen. Used in conjunction with the {@link ConfigurationManagerSpiHandler} annotation.
 */
@Documented
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@PublicSpi
public @interface ОbjectsEnumeratedBy {

	/**
	 * @return the implementation of {@link AppDataObjectEnumerator} which is responsible for getting the app's object types and objects.
	 */
	Class<? extends AppDataObjectEnumerator> value();
}
