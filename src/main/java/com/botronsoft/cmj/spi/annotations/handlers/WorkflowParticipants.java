package com.botronsoft.cmj.spi.annotations.handlers;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Container annotation for the {@link HandlesWorkflowParticipant} annotation.
 *
 * @see HandlesWorkflowParticipant
 */
@Target(value = ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface WorkflowParticipants {

	HandlesWorkflowParticipant[] value();
}
