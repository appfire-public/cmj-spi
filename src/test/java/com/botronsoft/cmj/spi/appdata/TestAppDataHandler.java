package com.botronsoft.cmj.spi.appdata;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.botronsoft.cmj.spi.annotations.handlers.ConfigurationManagerSpiHandler;
import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceCollector;
import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceLookup;
import com.botronsoft.cmj.spi.configuration.ExportContext;
import com.botronsoft.cmj.spi.configuration.ImportContext;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.link.IssueLinkType;
import com.atlassian.jira.issue.priority.Priority;
import com.atlassian.jira.issue.resolution.Resolution;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.security.IssueSecurityLevel;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.roles.ProjectRole;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.query.Query;

@RunWith(MockitoJUnitRunner.class)
public class TestAppDataHandler {

	@Mock
	private ExportContext mockExportContext;

	@Mock
	private ImportContext mockImportContext;

	@Mock
	private ConfigurationReferenceCollector mockReferenceCollector;

	@Mock
	private ConfigurationReferenceLookup mockReferenceLookup;

	@Mock
	private Field field;

	@Mock
	private ApplicationUser user;

	@Mock
	private Group group;

	@Mock
	private IssueType issueType;

	@Mock
	private IssueLinkType issueLinkType;

	@Mock
	private Status status;

	@Mock
	private Resolution resolution;

	@Mock
	private Priority priority;

	@Mock
	private SearchRequest searchRequest;

	@Mock
	private Query query;

	@Mock
	private Project project;

	@Mock
	private ProjectRole projectRole;

	@Mock
	private IssueSecurityLevel securityLevel;

	@Before
	public void setUp() {
		when(mockExportContext.getReferenceCollector()).thenReturn(mockReferenceCollector);
		when(mockImportContext.getReferenceLookup()).thenReturn(mockReferenceLookup);
	}

	@Test
	public void apiBackwardCompatibility() {
		AppDataHandler handler = new SampleAppDataHandler();
		Map<String, List<String>> objects = new HashMap<>();
		objects.put("typeId1", Lists.newArrayList("id1", "id2"));
		objects.put("typeId2", Lists.newArrayList("id1"));

		Optional<String> exportedData = handler.exportData(objects, mockExportContext);
		Assert.assertThat(exportedData.isPresent(), is(true));
		handler.importData(exportedData.get(), mockImportContext);
	}

	@ConfigurationManagerSpiHandler()
	private class SampleAppDataHandler implements AppDataHandler {
		private final Logger log = Logger.getLogger(SampleAppDataHandler.class);

		@Override
		public Optional<String> exportData(Map<String, List<String>> selectedObjectIds, ExportContext exportContext) {
			ConfigurationReferenceCollector referenceCollector = exportContext.getReferenceCollector();

			for (String typeId : selectedObjectIds.keySet()) {
				for (String objectId : selectedObjectIds.get(typeId)) {
					referenceCollector.collectField("test-" + typeId + "-" + objectId, field);
					referenceCollector.collectUser("test-" + typeId + "-" + objectId, user);
					referenceCollector.collectGroup("test-" + typeId + "-" + objectId, group);
					referenceCollector.collectIssueType("test-" + typeId + "-" + objectId, issueType);
					referenceCollector.collectIssueLinkType("test-" + typeId + "-" + objectId, issueLinkType);
					referenceCollector.collectStatus("test-" + typeId + "-" + objectId, status);
					referenceCollector.collectResolution("test-" + typeId + "-" + objectId, resolution);
					referenceCollector.collectPriority("test-" + typeId + "-" + objectId, priority);
					referenceCollector.collectSearchRequest("test-" + typeId + "-" + objectId, searchRequest);
					referenceCollector.collectQuery("test-" + typeId + "-" + objectId, query);
					referenceCollector.collectProject("test-" + typeId + "-" + objectId, project);
					referenceCollector.collectProjectRole("test-" + typeId + "-" + objectId, projectRole);
					referenceCollector.collectSecurityLevel("test-" + typeId + "-" + objectId, securityLevel);
				}
			}

			return selectedObjectIds.isEmpty() ? Optional.empty() : Optional.of(serialize(selectedObjectIds));
		}

		@Override
		public void importData(String data, ImportContext importContext) {
			Map<String, List<String>> selectedObjectIds = deserializeAppData(data);

			ConfigurationReferenceLookup referenceLookup = importContext.getReferenceLookup();

			for (String typeId : selectedObjectIds.keySet()) {
				for (String objectId : selectedObjectIds.get(typeId)) {
					referenceLookup.lookupField("test-" + typeId + "-" + objectId);
					referenceLookup.lookupUser("test-" + typeId + "-" + objectId);
					referenceLookup.lookupGroup("test-" + typeId + "-" + objectId);
					referenceLookup.lookupIssueType("test-" + typeId + "-" + objectId);
					referenceLookup.lookupIssueLinkType("test-" + typeId + "-" + objectId);
					referenceLookup.lookupStatus("test-" + typeId + "-" + objectId);
					referenceLookup.lookupResolution("test-" + typeId + "-" + objectId);
					referenceLookup.lookupPriority("test-" + typeId + "-" + objectId);
					referenceLookup.lookupSearchRequest("test-" + typeId + "-" + objectId);
					referenceLookup.lookupQuery("test-" + typeId + "-" + objectId);
					referenceLookup.lookupProject("test-" + typeId + "-" + objectId);
					referenceLookup.lookupProjectRole("test-" + typeId + "-" + objectId);
					referenceLookup.lookupSecurityLevel("test-" + typeId + "-" + objectId);
				}
			}
		}

		public String serialize(Object config) {
			if (config == null) {
				throw new IllegalArgumentException("null config not allowed");
			}
			return new Gson().toJson(config);
		}

		public Map<String, List<String>> deserializeAppData(String rawAppData) {
			if (Strings.isNullOrEmpty(rawAppData)) {
				throw new IllegalArgumentException("null or empty issue data not allowed");
			}
			return new GsonBuilder().setPrettyPrinting().create().fromJson(rawAppData, new TypeToken<Map<String, ArrayList<String>>>() {
			}.getType());
		}
	}
}
