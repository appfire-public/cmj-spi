package com.botronsoft.cmj.spi.appdata;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

import java.util.List;

import javax.annotation.Nullable;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.Lists;

@RunWith(MockitoJUnitRunner.class)
public class TestAppDataObjectEnumerator {
	private final static String icon = "data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCEtLSBHZW5lcmF0b3I6IEFkb2JlIElsbHVzdHJhdG9yIDE2LjAuMywgU1ZHIEV4cG9ydCBQbHVnLUluIC4gU1ZHIFZlcnNpb246IDYuMDAgQnVpbGQgMCkgIC0tPgo8IURPQ1RZUEUgc3ZnIFBVQkxJQyAiLS8vVzNDLy9EVEQgU1ZHIDEuMS8vRU4iICJodHRwOi8vd3d3LnczLm9yZy9HcmFwaGljcy9TVkcvMS4xL0RURC9zdmcxMS5kdGQiPgo8c3ZnIHZlcnNpb249IjEuMSIgaWQ9IkxheWVyXzIiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4IgoJIHdpZHRoPSIyNTZweCIgaGVpZ2h0PSIyNTZweCIgdmlld0JveD0iMCAwIDI1NiAyNTYiIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDI1NiAyNTYiIHhtbDpzcGFjZT0icHJlc2VydmUiPgo8Y2lyY2xlIGZpbGw9IiNDQ0NDQ0MiIGN4PSIxMjgiIGN5PSIxMjgiIHI9IjEyOCIvPgo8L3N2Zz4K";

	@Test
	public void apiBackwardCompatibility() {
		AppDataObjectEnumerator enumerator = new SampleAppDataObjectEnumerator();

		List<AppDataObjectType> types = enumerator.getObjectTypes();
		assertThat(types.size(), is(3));
		assertThat(types.get(0).getId(), is("typeOneId"));
		assertThat(types.get(0).getName(), is("typeOne"));
		assertThat(types.get(0).getIcon(), is(icon));
		assertThat(types.get(1).getId(), is("typeTwoId"));
		assertThat(types.get(1).getName(), is("typeTwo"));
		assertThat(types.get(1).getIcon(), is(icon));
		assertThat(types.get(2).getId(), is("typeThreeId"));
		assertThat(types.get(2).getName(), is("typeThree"));
		assertNull(types.get(2).getIcon());

		List<AppDataObject> objectsTypeOne = enumerator.getObjects("typeOneId");
		assertThat(objectsTypeOne.size(), is(2));
		assertAppObjectsAreCorrect(objectsTypeOne, 0, "typeOneId", "id1", "name1", "description1");
		assertAppObjectsAreCorrect(objectsTypeOne, 1, "typeOneId", "id2", "name2", "description2");

		List<AppDataObject> objectsTypeTwo = enumerator.getObjects("typeTwoId");
		assertThat(objectsTypeTwo.size(), is(3));
		assertAppObjectsAreCorrect(objectsTypeTwo, 0, "typeTwoId", "id3", "name3", "description3");
		assertAppObjectsAreCorrect(objectsTypeTwo, 1, "typeTwoId", "id4", "name4", "description4");
		assertAppObjectsAreCorrect(objectsTypeTwo, 2, "typeTwoId", "id5", "name5", "description5");

		List<AppDataObject> objectsTypeThree = enumerator.getObjects("typeThreeId");
		assertThat(objectsTypeThree.size(), is(2));
		assertAppObjectsAreCorrect(objectsTypeThree, 0, "typeThreeId", "id6", "name6", "description6");
		assertAppObjectsAreCorrect(objectsTypeThree, 1, "typeThreeId", "id7", "name7", "description7");
	}

	private void assertAppObjectsAreCorrect(List<AppDataObject> objects, int position, String typeId, String objectId, String name,
			String description) {
		assertThat(objects.get(position).getTypeId(), is(typeId));
		assertThat(objects.get(position).getObjectId(), is(objectId));
		assertThat(objects.get(position).getName(), is(name));
		assertThat(objects.get(position).getDescription(), is(description));
	}

	private static class SampleAppDataObjectEnumerator implements AppDataObjectEnumerator {
		@Override
		public List<AppDataObjectType> getObjectTypes() {
			return Lists.newArrayList(new SampleAppDataObjectType("typeOneId", "typeOne", icon),
					new SampleAppDataObjectType("typeTwoId", "typeTwo", icon), new SampleAppDataObjectType("typeThreeId", "typeThree"));
		}

		@Override
		public List<AppDataObject> getObjects(String typeId) {
			switch (typeId) {
			case "typeOneId":
				return Lists.newArrayList(new SampleAppDataObject("typeOneId", "id1", "name1", "description1"),
						new SampleAppDataObject("typeOneId", "id2", "name2", "description2"));
			case "typeTwoId":
				return Lists.newArrayList(new SampleAppDataObject("typeTwoId", "id3", "name3", "description3"),
						new SampleAppDataObject("typeTwoId", "id4", "name4", "description4"),
						new SampleAppDataObject("typeTwoId", "id5", "name5", "description5"));

			case "typeThreeId":
				return Lists.newArrayList(new SampleAppDataObject("typeThreeId", "id6", "name6", "description6"),
						new SampleAppDataObject("typeThreeId", "id7", "name7", "description7"));
			}
			return Lists.newArrayList();
		}
	}

	private static class SampleAppDataObject implements AppDataObject {
		private final String typeId;
		private final String objectId;
		private final String name;
		private final String description;

		public SampleAppDataObject(String typeId, String objectId, String name, String description) {
			this.typeId = typeId;
			this.objectId = objectId;
			this.name = name;
			this.description = description;
		}

		@Override
		public String getTypeId() {
			return typeId;
		}

		@Override
		public String getObjectId() {
			return objectId;
		}

		@Override
		public String getName() {
			return name;
		}

		@Override
		public String getDescription() {
			return description;
		}
	}

	private static class SampleAppDataObjectType implements AppDataObjectType {

		private final String id;
		private final String name;
		private String icon;

		public SampleAppDataObjectType(String id, String name) {
			this.id = id;
			this.name = name;
		}

		public SampleAppDataObjectType(String id, String name, String icon) {
			this(id, name);
			this.icon = icon;
		}

		@Override
		public String getId() {
			return id;
		}

		@Override
		public String getName() {
			return name;
		}

		@Nullable
		@Override
		public String getIcon() {
			return icon;
		}
	}
}
