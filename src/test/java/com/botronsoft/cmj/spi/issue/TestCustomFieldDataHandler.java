package com.botronsoft.cmj.spi.issue;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceCollector;
import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceLookup;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.CustomField;

@RunWith(MockitoJUnitRunner.class)
public class TestCustomFieldDataHandler {

	@Mock
	private IssueExportContext mockExportContext;

	@Mock
	private IssueImportContext mockImportContext;

	@Mock
	private ConfigurationReferenceCollector mockReferenceCollector;

	@Mock
	private ConfigurationReferenceLookup mockReferenceLookup;

	@Before
	public void setUp() {
		when(mockExportContext.getReferenceCollector()).thenReturn(mockReferenceCollector);
		when(mockImportContext.getReferenceLookup()).thenReturn(mockReferenceLookup);
	}

	@Test
	public void apiBackwardCompatibility() {
		CustomFieldDataHandler<CustomData> handler = new SampleHandler();

		assertThat(Optional.of("15"), is(handler.transformIssueValueForExport(null, null, new CustomData(15), mockExportContext)));
		assertThat(Optional.of("27"), is(handler.transformChangeLogValueForExport(null, null, "27", null, mockExportContext)));
		assertThat(18, is(handler.transformIssueValueForImport(null, null, "18", mockImportContext).get().getSomeData()));
	}

	/**
	 * Be very careful when modifying this class. In most cases a change here will mean that the SPI backward compatibility has been broken.
	 */
	private static class SampleHandler implements CustomFieldDataHandler<CustomData> {

		@Override
		public Optional<String> transformIssueValueForExport(CustomField customField, Issue issue, CustomData value,
				IssueExportContext exportContext) {
			exportContext.getReferenceCollector().collectField("test", null);
			exportContext.getReferenceCollector().collectUser("test", null);
			exportContext.getReferenceCollector().collectGroup("test", null);
			exportContext.getReferenceCollector().collectIssueType("test", null);
			exportContext.getReferenceCollector().collectIssueLinkType("test", null);
			exportContext.getReferenceCollector().collectStatus("test", null);
			exportContext.getReferenceCollector().collectResolution("test", null);
			exportContext.getReferenceCollector().collectPriority("test", null);
			exportContext.getReferenceCollector().collectSearchRequest("test", null);
			exportContext.getReferenceCollector().collectQuery("test", null);
			exportContext.getReferenceCollector().collectProject("test", null);
			exportContext.getReferenceCollector().collectProjectRole("test", null);
			exportContext.getReferenceCollector().collectRequestType("test", null);
			exportContext.getReferenceCollector().collectSecurityLevel("test", null);

			return Optional.of(Integer.toString(value.getSomeData()));
		}

		@Override
		public Optional<String> transformChangeLogValueForExport(CustomField customField, Issue issue, String value, String valueString,
				IssueExportContext exportContext) {
			exportContext.getReferenceCollector().collectField("test", null);
			exportContext.getReferenceCollector().collectUser("test", null);
			exportContext.getReferenceCollector().collectGroup("test", null);
			exportContext.getReferenceCollector().collectIssueType("test", null);
			exportContext.getReferenceCollector().collectIssueLinkType("test", null);
			exportContext.getReferenceCollector().collectStatus("test", null);
			exportContext.getReferenceCollector().collectResolution("test", null);
			exportContext.getReferenceCollector().collectPriority("test", null);
			exportContext.getReferenceCollector().collectSearchRequest("test", null);
			exportContext.getReferenceCollector().collectQuery("test", null);
			exportContext.getReferenceCollector().collectProject("test", null);
			exportContext.getReferenceCollector().collectProjectRole("test", null);
			exportContext.getReferenceCollector().collectRequestType("test", null);
			exportContext.getReferenceCollector().collectSecurityLevel("test", null);

			return Optional.of(value);
		}

		@Override
		public Optional<CustomData> transformIssueValueForImport(CustomField customField, Issue issue, String value,
				IssueImportContext importContext) {
			importContext.getReferenceLookup().lookupField("test");
			importContext.getReferenceLookup().lookupUser("test");
			importContext.getReferenceLookup().lookupGroup("test");
			importContext.getReferenceLookup().lookupIssueType("test");
			importContext.getReferenceLookup().lookupIssueLinkType("test");
			importContext.getReferenceLookup().lookupStatus("test");
			importContext.getReferenceLookup().lookupResolution("test");
			importContext.getReferenceLookup().lookupPriority("test");
			importContext.getReferenceLookup().lookupSearchRequest("test");
			importContext.getReferenceLookup().lookupQuery("test");
			importContext.getReferenceLookup().lookupProject("test");
			importContext.getReferenceLookup().lookupProjectRole("test");
			importContext.getReferenceLookup().lookupRequestType("test");
			importContext.getReferenceLookup().lookupSecurityLevel("test");

			return Optional.of(new CustomData(Integer.parseInt(value)));
		}
	}

	private static final class CustomData {

		private final int someData;

		public CustomData(int someData) {
			this.someData = someData;
		}

		public int getSomeData() {
			return someData;
		}
	}
}
