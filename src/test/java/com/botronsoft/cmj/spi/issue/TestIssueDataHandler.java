package com.botronsoft.cmj.spi.issue;

import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.Optional;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceCollector;
import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceLookup;

import com.atlassian.jira.project.Project;

@RunWith(MockitoJUnitRunner.class)
public class TestIssueDataHandler {

	@Mock
	private IssueExportContext mockExportContext;

	@Mock
	private IssueImportContext mockImportContext;

	@Mock
	private ConfigurationReferenceCollector mockReferenceCollector;

	@Mock
	private ConfigurationReferenceLookup mockReferenceLookup;

	@Mock
	private Project mockProject;

	@Before
	public void setUp() {
		when(mockExportContext.getReferenceCollector()).thenReturn(mockReferenceCollector);
		when(mockImportContext.getReferenceLookup()).thenReturn(mockReferenceLookup);
	}

	@Test
	public void apiBackwardCompatibility() {
		IssueDataHandler handler = new SampleHandler();

		Optional<String> issueData = handler.exportIssueData(mockProject, Collections.singleton(1l), mockExportContext);
		handler.importIssueData(issueData.get(), mockProject, mockImportContext);
	}

	/**
	 * Be very careful when modifying this class. In most cases a change here will mean that the SPI backward compatibility has been broken.
	 */
	private static class SampleHandler implements IssueDataHandler {

		@Override
		public Optional<String> exportIssueData(Project project, Set<Long> issueIds, IssueExportContext exportContext) {
			exportContext.getReferenceCollector().collectField("test", null);
			exportContext.getReferenceCollector().collectUser("test", null);
			exportContext.getReferenceCollector().collectGroup("test", null);
			exportContext.getReferenceCollector().collectIssueType("test", null);
			exportContext.getReferenceCollector().collectIssueLinkType("test", null);
			exportContext.getReferenceCollector().collectStatus("test", null);
			exportContext.getReferenceCollector().collectResolution("test", null);
			exportContext.getReferenceCollector().collectPriority("test", null);
			exportContext.getReferenceCollector().collectSearchRequest("test", null);
			exportContext.getReferenceCollector().collectQuery("test", null);
			exportContext.getReferenceCollector().collectProject("test", null);
			exportContext.getReferenceCollector().collectProjectRole("test", null);
			exportContext.getReferenceCollector().collectRequestType("test", null);
			exportContext.getReferenceCollector().collectSecurityLevel("test", null);

			return Optional.of("test");
		}

		@Override
		public void importIssueData(String data, Project project, IssueImportContext importContext) {
			importContext.getReferenceLookup().lookupField("test");
			importContext.getReferenceLookup().lookupUser("test");
			importContext.getReferenceLookup().lookupGroup("test");
			importContext.getReferenceLookup().lookupIssueType("test");
			importContext.getReferenceLookup().lookupIssueLinkType("test");
			importContext.getReferenceLookup().lookupStatus("test");
			importContext.getReferenceLookup().lookupResolution("test");
			importContext.getReferenceLookup().lookupPriority("test");
			importContext.getReferenceLookup().lookupSearchRequest("test");
			importContext.getReferenceLookup().lookupQuery("test");
			importContext.getReferenceLookup().lookupProject("test");
			importContext.getReferenceLookup().lookupProjectRole("test");
			importContext.getReferenceLookup().lookupRequestType("test");
			importContext.getReferenceLookup().lookupSecurityLevel("test");
		}
	}
}
