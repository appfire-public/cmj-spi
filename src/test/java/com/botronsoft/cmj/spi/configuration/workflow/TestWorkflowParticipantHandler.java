package com.botronsoft.cmj.spi.configuration.workflow;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.botronsoft.cmj.spi.annotations.handlers.ConfigurationManagerSpiHandler;
import com.botronsoft.cmj.spi.annotations.handlers.HandlesWorkflowParticipant;
import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceCollector;
import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceLookup;
import com.botronsoft.cmj.spi.configuration.ExportContext;
import com.botronsoft.cmj.spi.configuration.ImportContext;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.link.IssueLinkType;
import com.atlassian.jira.issue.priority.Priority;
import com.atlassian.jira.issue.resolution.Resolution;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.security.IssueSecurityLevel;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.roles.ProjectRole;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.query.Query;

@RunWith(MockitoJUnitRunner.class)
public class TestWorkflowParticipantHandler {

	@Mock
	private ExportContext mockExportContext;

	@Mock
	private ImportContext mockImportContext;

	@Mock
	private ConfigurationReferenceCollector mockReferenceCollector;

	@Mock
	private ConfigurationReferenceLookup mockReferenceLookup;

	@Mock
	private Field field;

	@Mock
	private ApplicationUser user;

	@Mock
	private Group group;

	@Mock
	private IssueType issueType;

	@Mock
	private IssueLinkType issueLinkType;

	@Mock
	private Status status;

	@Mock
	private Resolution resolution;

	@Mock
	private Priority priority;

	@Mock
	private SearchRequest searchRequest;

	@Mock
	private Query query;

	@Mock
	private Project project;

	@Mock
	private ProjectRole projectRole;

	@Mock
	private IssueSecurityLevel securityLevel;

	Map<String, String> initialArgs = new HashMap<>();

	@Before
	public void setUp() {
		when(mockExportContext.getReferenceCollector()).thenReturn(mockReferenceCollector);
		when(mockImportContext.getReferenceLookup()).thenReturn(mockReferenceLookup);

		initialArgs.put("fieldArg", "1");
		initialArgs.put("userArg", "2");
		initialArgs.put("groupArg", "3");
		initialArgs.put("issueTypeArg", "4");
		initialArgs.put("issueLinkTypeArg", "5");
		initialArgs.put("statusArg", "6");
		initialArgs.put("resolutionArg", "7");
		initialArgs.put("priorityArg", "8");
		initialArgs.put("searchRequestArg", "9");
		initialArgs.put("queryArg", "10");
		initialArgs.put("projectArg", "11");
		initialArgs.put("projectRoleArg", "12");
		initialArgs.put("requestTypeArg", "13");
		initialArgs.put("securityLevelArg", "14");

		for (String key : initialArgs.keySet()) {
			switch (key) {
			case "fieldArg":
				when(mockReferenceLookup.lookupField("1")).thenReturn(Optional.of(field));
				when(field.getId()).thenReturn("11");
				break;
			case "userArg":
				when(mockReferenceLookup.lookupUser("2")).thenReturn(Optional.of(user));
				when(user.getId()).thenReturn(12l);
				break;
			case "groupArg":
				when(mockReferenceLookup.lookupGroup("3")).thenReturn(Optional.of(group));
				when(group.getName()).thenReturn("13");
			case "issueTypeArg":
				when(mockReferenceLookup.lookupIssueType("4")).thenReturn(Optional.of(issueType));
				when(issueType.getId()).thenReturn("14");
				break;
			case "issueLinkTypeArg":
				when(mockReferenceLookup.lookupIssueLinkType("5")).thenReturn(Optional.of(issueLinkType));
				when(issueLinkType.getId()).thenReturn(15l);
				break;
			case "statusArg":
				when(mockReferenceLookup.lookupStatus("6")).thenReturn(Optional.of(status));
				when(status.getId()).thenReturn("16");
				break;
			case "resolutionArg":
				when(mockReferenceLookup.lookupResolution("7")).thenReturn(Optional.of(resolution));
				when(resolution.getId()).thenReturn("17");
				break;
			case "priorityArg":
				when(mockReferenceLookup.lookupPriority("8")).thenReturn(Optional.of(priority));
				when(priority.getId()).thenReturn("18");
				break;
			case "searchRequestArg":
				when(mockReferenceLookup.lookupSearchRequest("9")).thenReturn(Optional.of(searchRequest));
				when(searchRequest.getId()).thenReturn(19l);
				break;
			case "queryArg":
				when(mockReferenceLookup.lookupQuery("10")).thenReturn(Optional.of(query));
				when(query.getQueryString()).thenReturn("20");
				break;
			case "projectArg":
				when(mockReferenceLookup.lookupProject("11")).thenReturn(Optional.of(project));
				when(project.getId()).thenReturn(21l);
			case "projectRoleArg":
				when(mockReferenceLookup.lookupProjectRole("12")).thenReturn(Optional.of(projectRole));
				when(projectRole.getId()).thenReturn(22l);
				break;
			case "requestTypeArg":
				when(mockReferenceLookup.lookupRequestType("13")).thenReturn(Optional.of(23L));
				break;
			case "securityLevelArg":
				when(mockReferenceLookup.lookupSecurityLevel("14")).thenReturn(Optional.of(securityLevel));
				when(securityLevel.getId()).thenReturn(24l);
				break;
			default:

			}
		}
	}

	@Test
	public void apiBackwardCompatibility() {
		SampleHandler handler = new SampleHandler();

		Map<String, String> resultArguments = handler
				.transformArgumentsForExport("com.atlassian.jira.workflow.condition.PermissionCondition", initialArgs, mockExportContext);

		Map<String, String> result = handler.transformArgumentsForImport("com.atlassian.jira.workflow.condition.PermissionCondition",
				resultArguments, mockImportContext);

		Assert.assertThat(result.get("fieldArg"), equalTo("11"));
		Assert.assertThat(result.get("userArg"), equalTo("12"));
		Assert.assertThat(result.get("groupArg"), equalTo("13"));
		Assert.assertThat(result.get("issueTypeArg"), equalTo("14"));
		Assert.assertThat(result.get("issueLinkTypeArg"), equalTo("15"));
		Assert.assertThat(result.get("statusArg"), equalTo("16"));
		Assert.assertThat(result.get("resolutionArg"), equalTo("17"));
		Assert.assertThat(result.get("priorityArg"), equalTo("18"));
		Assert.assertThat(result.get("searchRequestArg"), equalTo("19"));
		Assert.assertThat(result.get("queryArg"), equalTo("20"));
		Assert.assertThat(result.get("projectArg"), equalTo("21"));
		Assert.assertThat(result.get("projectRoleArg"), equalTo("22"));
		Assert.assertThat(result.get("requestTypeArg"), equalTo("23"));
		Assert.assertThat(result.get("securityLevelArg"), equalTo("24"));

	}

	@ConfigurationManagerSpiHandler
	@HandlesWorkflowParticipant(className = "com.botronsoft.test.Condition")
	@HandlesWorkflowParticipant(className = "com.botronsoft.test.Validator")
	@HandlesWorkflowParticipant(className = "com.botronsoft.test.PostFunction")
	private class SampleHandler implements WorkflowParticipantHandler {

		@Override
		public Map<String, String> transformArgumentsForExport(String className, Map<String, String> args, ExportContext exportContext) {

			for (String key : args.keySet()) {
				switch (key) {
				case "fieldArg":
					exportContext.getReferenceCollector().collectField(args.get(key), null);
					break;
				case "userArg":
					exportContext.getReferenceCollector().collectUser(args.get(key), null);
					break;
				case "groupArg":
					exportContext.getReferenceCollector().collectGroup(args.get(key), null);
					break;
				case "issueTypeArg":
					exportContext.getReferenceCollector().collectIssueType(args.get(key), null);
					break;
				case "issueLinkTypeArg":
					exportContext.getReferenceCollector().collectIssueLinkType(args.get(key), null);
					break;
				case "statusArg":
					exportContext.getReferenceCollector().collectStatus(args.get(key), null);
					break;
				case "resolutionArg":
					exportContext.getReferenceCollector().collectResolution(args.get(key), null);
					break;
				case "priorityArg":
					exportContext.getReferenceCollector().collectPriority(args.get(key), null);
					break;
				case "searchRequestArg":
					exportContext.getReferenceCollector().collectSearchRequest(args.get(key), null);
					break;
				case "queryArg":
					exportContext.getReferenceCollector().collectQuery(args.get(key), null);
					break;
				case "projectArg":
					exportContext.getReferenceCollector().collectProject(args.get(key), null);
					break;
				case "projectRoleArg":
					exportContext.getReferenceCollector().collectProjectRole(args.get(key), null);
					break;
				case "requestTypeArg":
					exportContext.getReferenceCollector().collectRequestType(args.get(key), null);
					break;
				case "securityLevelArg":
					exportContext.getReferenceCollector().collectSecurityLevel(args.get(key), null);
					break;
				default:

				}
			}

			return new HashMap<>(args);
		}

		@Override
		public Map<String, String> transformArgumentsForImport(String className, Map<String, String> args, ImportContext importContext) {

			Map<String, String> resultArguments = new HashMap<>();

			for (String key : args.keySet()) {
				switch (key) {
				case "fieldArg":
					Optional<Field> field = importContext.getReferenceLookup().lookupField(args.get(key));
					resultArguments.put(key, String.valueOf(field.get().getId()));
					break;
				case "userArg":
					Optional<ApplicationUser> user = importContext.getReferenceLookup().lookupUser(args.get(key));
					resultArguments.put(key, String.valueOf(user.get().getId()));
					break;
				case "groupArg":
					Optional<Group> group = importContext.getReferenceLookup().lookupGroup(args.get(key));
					resultArguments.put(key, String.valueOf(group.get().getName()));
					break;
				case "issueTypeArg":
					Optional<IssueType> issueType = importContext.getReferenceLookup().lookupIssueType(args.get(key));
					resultArguments.put(key, String.valueOf(issueType.get().getId()));
					break;
				case "issueLinkTypeArg":
					Optional<IssueLinkType> issueLinkType = importContext.getReferenceLookup().lookupIssueLinkType(args.get(key));
					resultArguments.put(key, String.valueOf(issueLinkType.get().getId()));
					break;
				case "statusArg":
					Optional<Status> status = importContext.getReferenceLookup().lookupStatus(args.get(key));
					resultArguments.put(key, String.valueOf(status.get().getId()));
					break;
				case "resolutionArg":
					Optional<Resolution> resolution = importContext.getReferenceLookup().lookupResolution(args.get(key));
					resultArguments.put(key, String.valueOf(resolution.get().getId()));
					break;
				case "priorityArg":
					Optional<Priority> priority = importContext.getReferenceLookup().lookupPriority(args.get(key));
					resultArguments.put(key, String.valueOf(priority.get().getId()));
					break;
				case "searchRequestArg":
					Optional<SearchRequest> searchRequest = importContext.getReferenceLookup().lookupSearchRequest(args.get(key));
					resultArguments.put(key, String.valueOf(searchRequest.get().getId()));
					break;
				case "queryArg":
					Optional<Query> query = importContext.getReferenceLookup().lookupQuery(args.get(key));
					resultArguments.put(key, String.valueOf(query.get().getQueryString()));
					break;
				case "projectArg":
					Optional<Project> project = importContext.getReferenceLookup().lookupProject(args.get(key));
					resultArguments.put(key, String.valueOf(project.get().getId()));
					break;
				case "projectRoleArg":
					Optional<ProjectRole> role = importContext.getReferenceLookup().lookupProjectRole(args.get(key));
					resultArguments.put(key, String.valueOf(role.get().getId()));
					break;
				case "requestTypeArg":
					Optional<Long> id = importContext.getReferenceLookup().lookupRequestType(args.get(key));
					resultArguments.put(key, String.valueOf(id.get()));
					break;
				case "securityLevelArg":
					Optional<IssueSecurityLevel> securityLevel = importContext.getReferenceLookup().lookupSecurityLevel(args.get(key));
					resultArguments.put(key, String.valueOf(securityLevel.get().getId()));
					break;
				default:

				}

			}

			return resultArguments;
		}
	}
}
