package com.botronsoft.cmj.spi.configuration.project;

import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.botronsoft.cmj.spi.annotations.handlers.ConfigurationManagerSpiHandler;
import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceCollector;
import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceLookup;
import com.botronsoft.cmj.spi.configuration.ExportContext;
import com.botronsoft.cmj.spi.configuration.ImportContext;

import com.atlassian.jira.project.Project;

@RunWith(MockitoJUnitRunner.class)
public class TestProjectConfigurationHandler {

	@Mock
	private ExportContext mockExportContext;

	@Mock
	private ImportContext mockImportContext;

	@Mock
	private ConfigurationReferenceCollector mockReferenceCollector;

	@Mock
	private ConfigurationReferenceLookup mockReferenceLookup;

	@Mock
	private Project mockProject;

	@Before
	public void setUp() {
		when(mockExportContext.getReferenceCollector()).thenReturn(mockReferenceCollector);
		when(mockImportContext.getReferenceLookup()).thenReturn(mockReferenceLookup);
	}

	@Test
	public void apiBackwardCompatibility() {
		ProjectConfigurationHandler handler = new SampleHandler();

		handler.exportConfiguration(mockProject, mockExportContext);
		handler.importConfiguration("test", mockProject, mockImportContext);
		handler.deleteConfiguration(mockProject);
	}

	/**
	 * Be very careful when modifying this class. In most cases a change here will mean that the SPI backward compatibility has been broken.
	 */
	@ConfigurationManagerSpiHandler
	private static class SampleHandler implements ProjectConfigurationHandler {

		@Override
		public Optional<String> exportConfiguration(Project project, ExportContext exportContext) {
			exportContext.getReferenceCollector().collectField("test", null);
			exportContext.getReferenceCollector().collectUser("test", null);
			exportContext.getReferenceCollector().collectGroup("test", null);
			exportContext.getReferenceCollector().collectIssueType("test", null);
			exportContext.getReferenceCollector().collectIssueLinkType("test", null);
			exportContext.getReferenceCollector().collectStatus("test", null);
			exportContext.getReferenceCollector().collectResolution("test", null);
			exportContext.getReferenceCollector().collectPriority("test", null);
			exportContext.getReferenceCollector().collectSearchRequest("test", null);
			exportContext.getReferenceCollector().collectQuery("test", null);
			exportContext.getReferenceCollector().collectProject("test", null);
			exportContext.getReferenceCollector().collectProjectRole("test", null);
			exportContext.getReferenceCollector().collectRequestType("test", null);
			exportContext.getReferenceCollector().collectSecurityLevel("test", null);

			return Optional.of("test");
		}

		@Override
		public void importConfiguration(String configuration, Project project, ImportContext importContext) {
			importContext.getReferenceLookup().lookupField("test");
			importContext.getReferenceLookup().lookupUser("test");
			importContext.getReferenceLookup().lookupGroup("test");
			importContext.getReferenceLookup().lookupIssueType("test");
			importContext.getReferenceLookup().lookupIssueLinkType("test");
			importContext.getReferenceLookup().lookupStatus("test");
			importContext.getReferenceLookup().lookupResolution("test");
			importContext.getReferenceLookup().lookupPriority("test");
			importContext.getReferenceLookup().lookupSearchRequest("test");
			importContext.getReferenceLookup().lookupQuery("test");
			importContext.getReferenceLookup().lookupProject("test");
			importContext.getReferenceLookup().lookupProjectRole("test");
			importContext.getReferenceLookup().lookupRequestType("test");
			importContext.getReferenceLookup().lookupSecurityLevel("test");
		}

		@Override
		public void deleteConfiguration(Project project) {

		}
	}

}
